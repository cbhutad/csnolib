package com.mycompany.app.model;

public class Refset {

    protected String refSetId;

    protected String moduleId;

    protected String refsetType;

    private Concept refsetConcept;

    private String refsetCount;

    private int resetActive;

    public String getRefSetId() {
        return refSetId;
    }

    public void setRefSetId(String refSetId) {
        this.refSetId = refSetId;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getRefsetType() {
        return refsetType;
    }

    public void setRefsetType(String refsetType) {
        this.refsetType = refsetType;
    }

    public Concept getRefsetConcept() {
        return refsetConcept;
    }

    public void setRefsetConcept(Concept refsetConcept) {
        this.refsetConcept = refsetConcept;
    }

    public String getRefsetCount() {
        return refsetCount;
    }

    public void setRefsetCount(String refsetCount) {
        this.refsetCount = refsetCount;
    }

    public int getResetActive() {
        return resetActive;
    }

    public void setResetActive(int resetActive) {
        this.resetActive = resetActive;
    }
    
}
