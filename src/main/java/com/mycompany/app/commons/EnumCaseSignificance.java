package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumCaseSignificance {
    
    INITIAL_CHAR_CASE_INSENSITIVE("900000000000020002"),
	CASE_SENSITIVE("900000000000017005"),
	CASE_INSENSITIVE("900000000000448009");
	
	private static final Map<String, EnumCaseSignificance> lookup = new ConcurrentHashMap<String, EnumCaseSignificance>();
    static 
    {
        for (EnumCaseSignificance d : EnumCaseSignificance.values())
            lookup.put(d.getValue(), d);
    }
    	
	private String value;
	
	private EnumCaseSignificance(String value) {
		this.value = value;
	}
	
	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
 
	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumCaseSignificance} enumeration for given string value
	 */
	public static EnumCaseSignificance getEnum(String val) {
		return lookup.get(val);
	}

}
