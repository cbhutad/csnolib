package com.mycompany.app.model;

/**
 * {@link CompositeDescription} is extended from {@link Description}.
 *  A {@link CompositeDescription} consists
 * of inherited fields of {@link Description} and 
 * description hierarchy,isPreferredTerm,conceptState,conceptFsn,definitionStatus.
 * 
 */
public class CompositeDescription extends Description {
   
    /**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Semantic Tag/ Hierarchy to which the description belongs
	 */
	private String hierarchy;
	/**
	 * String value that describes whether the description is preferred term or not
	 */
    private String isPreferredTerm;
    /**
     * It refers to the state of the Concept to which the particular
     * description is associated
     */
	private String conceptState;
	/**
	 * Fully Specified Name of the Concept
	 */
	private String conceptFsn;
	/**
	 * Definition status of the Concept to which the description is 
	 * associated
	 */
	private String definitionStatus;
	/**
	 * RefSetMember to identify description is from UK or US
	 */	
	private RefSetMember refSetLangMember;

    private Refset refset;

    /**
     * Default Constructor
     */
    public CompositeDescription() {
    }

    /**
	 * Returns the Semantic Tag/ hierarchy of a description.
	 * @return {@link String} value that specifies the Semantic Tag/hierarchy
	 * to which the description belongs.
	 */
    public String getHierarchy() {
        return hierarchy;
    }

    /**
	 * Returns the Semantic Tag/hierarchy of a description.
	 * @param hierachy
	 * 				the Semantic Tag/ hierarchy of the description.
	 */
    public void setHierarchy(String hierarchy) {
        this.hierarchy = hierarchy;
    }

    /**
	 * Returns a String value to identify whether a description is 
	 * preferred or acceptable.
	 * @return {@link String} value that specifies the preferred state of a
	 * description.
	 */
    public String getIsPreferredTerm() {
        return isPreferredTerm;
    }

    /**
	 * Sets the isPreferredTerm value.
	 * @param isPreferredTerm
	 * 						String value to identify whether a description is 
	 * 						preferred or not.
	 */
    public void setIsPreferredTerm(String isPreferredTerm) {
        this.isPreferredTerm = isPreferredTerm;
    }

    /**
	 * Returns the state of the concept corresponding to the description.
	 * @return {@link String} value that specifies the state of the concept
	 * associated with this description.
	 */
    public String getConceptState() {
        return conceptState;
    }

    /**
	 * Sets the concept state.
	 * @param conceptState 
	 * 						{@link String} value that specifies the state of the concept
	 * 						associated with this description.
	 */
    public void setConceptState(String conceptState) {
        this.conceptState = conceptState;
    }

    /**
	 * Returns the concept FSN.
	 * @return {@link String} value that corresponds to the FSN.
	 */
    public String getConceptFsn() {
        return conceptFsn;
    }

    /**
	 * Sets the concept FSN value.
	 * @param conceptFsn 
	 * 						{@link String} value that specifies the FSN of the 
	 * 						concept.
	 */
    public void setConceptFsn(String conceptFsn) {
        this.conceptFsn = conceptFsn;
    }

    /**
	 * Returns the definition status of the SNOMED CT {@link Concept}.
	 * associated with this description
	 * @return {@link String}.
	 */
    public String getDefinitionStatus() {
        return definitionStatus;
    }

    /**
	 * Sets the definition status.
	 * @param definitionStatus 
	 * 						{@link String} value that corresponds to
	 * 						 the definition status of the Concept.
	 */
    public void setDefinitionStatus(String definitionStatus) {
        this.definitionStatus = definitionStatus;
    }

    /**
	 * Used to get RefSetMember object
	 */
    public RefSetMember getRefSetLangMember() {
        return refSetLangMember;
    }

    /**
	 * Used to set RefSetMember object
	 */	
    public void setRefSetLangMember(RefSetMember refSetLangMember) {
        this.refSetLangMember = refSetLangMember;
    }


    public Refset getRefset() {
        return refset;
    }

    public void setRefset(Refset refset) {
        this.refset = refset;
    }

    @Override
    public String toString() {
        return "CompositeDescription [effectiveTime=" + effectiveTime + ", activeStatus=" + activeStatus + ", moduleId="
                + moduleId + ", hierarchy=" + hierarchy + ", id=" + id + ", isPreferredTerm=" + isPreferredTerm
                + ", conceptState=" + conceptState + ", conceptFsn=" + conceptFsn + ", definitionStatus="
                + definitionStatus + ", refSetLangMember=" + refSetLangMember + "]";
    }
    
}
