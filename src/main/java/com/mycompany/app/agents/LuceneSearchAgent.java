package com.mycompany.app.agents;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import com.mycompany.app.api.ISearchAgent;
import com.mycompany.app.commons.Constants;
import com.mycompany.app.commons.EnumDirection;
import com.mycompany.app.factory.IndexReaderFactory;
import com.mycompany.app.model.CompositeDescription;
import com.mycompany.app.model.Concept;
import com.mycompany.app.model.Refset;
import com.mycompany.app.model.Relationship;
import com.mycompany.app.model.RelationshipTC;
import com.mycompany.app.model.SimpleMapRefsetResult;
import com.mycompany.app.util.LoggerUtility;
import com.mycompany.app.util.PropertyReader;

public class LuceneSearchAgent implements ISearchAgent {

    private static final String STATEMENT_IS_NOT_CLOSED = "Statement is not closed";
    private static final String INVALID_PARAMETER = "INVALID PARAMETER";
    private static final String E_MMM_DD_HH_MM_SS_Z_YYYY = "E MMM dd HH:mm:ss Z yyyy";
    private static final String AND = " AND ";
    private static final String PREFERRED_TERM_NOT_FOUND_FOR_SCTID = "preferred term not found for SCTID";
    private static final String INDEX_GENERATION_PROCESS_COMPLETED = "index generation process completed !!!";
    private static final String NUMBER_OF_RECORDS_WHICH_ARE_INDEXED_TILL_NOW = "Number of records which are indexed till now : ";
    private static final String TOTAL_NUMBER_OF = "Total number of ";
    private static final String INDEX_CREATIOn_FAILED_FOR = "Index creation failed for";
    private static final String RECORDS_NEED_TO_BE_INDEXED = " records need to be indexed : ";
    private static final long serialVersionUID = 1L;
    private static final int RESPONSE_SIZE_FOR_EXTENSION = 100;

    private IndexWriter indexWriter = null;
    private Analyzer analyzer = null;
    private IndexWriterConfig indexConfig = null;
    private Directory indexDirectory = null;
    private static Set<String> semanticTagsForIndexing = new HashSet<>();
    private static List<String> relationTypesForIndexing = new ArrayList<>();
    private static HashMap<String, HashMap<Integer, Integer>> extensionIdMap = new HashMap<>();
    protected HashMap<String, String> relationTypeMap = new HashMap<>();

    private IndexReaderFactory indexReaderFactory = IndexReaderFactory.getInstance();

    /**
     * Returns {@link IndexWriter} object.
     * @param refsetId Identifier of refset
     * @return object of {@link IndexWriter}
     * @throws IOException if any error occurs while processing.
     */
    private IndexWriter getIndexWriter(String refsetId) throws IOException {
        if (this.indexWriter == null) {
            this.indexWriter = new IndexWriter(getIndexDirectoryToWrite(refsetId), getIndexConfig());
        }
        return this.indexWriter;
    }

    /**
     * Returns index directory location to write the files.
     * @param refsetId Identifier of Refset
     * @return {@link Directory}
     * @throws IOException if any error occurs while processing
     */
    private Directory getIndexDirectoryToWrite(String refsetId) throws IOException {
        Logger logger = LogManager.getLogger(LuceneSearchAgent.class);
        if (indexDirectory == null) {
            PropertyReader.loadSystemProperties();
            try {
                if (refsetId == null)
                    indexDirectory = FSDirectory.open(Paths.get(new File(PropertyReader.getProperty(Constants.DATA_DIR) + File.separator + Constants.SNOINDEX).getAbsolutePath()));
                if (Constants.SIMPLE_MAP_INDEX.equals(refsetId) || Constants.SNOINDEX.equals(refsetId) || Constants.RELATION_INDEX.equals(refsetId) || Constants.TCINDEX.equals(refsetId) || Constants.SEMINDEX.equals(refsetId) || Constants.REL_TYPE_INDEX.equalsIgnoreCase(refsetId))
                    indexDirectory = FSDirectory.open(Paths.get(new File(PropertyReader.getProperty(Constants.DATA_DIR) + File.separator + refsetId).getAbsolutePath()));
                else
                    indexDirectory = FSDirectory.open(Paths.get(new File(PropertyReader.getProperty(Constants.DATA_DIR) + File.separator + Constants.REFINDEX).getAbsolutePath()));
            } catch (IOException ex) {
                logger.info(LoggerUtility.Log, "Creating sub directory on WinNTFfilesystem");
                StackTraceElement[] arrStackTraceElements = ex.getStackTrace();
                if (arrStackTraceElements[0].getClassName().equals("java.io.WinNTFileSystem")) {
                    try {
                        if (refsetId == null || Constants.SIMPLE_MAP_INDEX.equals(refsetId) || Constants.SNOINDEX.equals(refsetId) || Constants.RELATION_INDEX.equals(refsetId) || Constants.TCINDEX.equals(refsetId) || Constants.SEMINDEX.equals(refsetId) || Constants.REL_TYPE_INDEX.equalsIgnoreCase(refsetId))
                            new File(PropertyReader.getProperty(Constants.DATA_DIR) + File.separator + refsetId).mkdir();
                        else
                            new File(PropertyReader.getProperty(Constants.DATA_DIR) + File.separator + Constants.REFINDEX).mkdir();
                        getIndexDirectoryToWrite(refsetId);
                    } catch (IOException ie) {
                        throw new IOException("Failed to create sub directory on WinNTFfilesystem");
                    }
                }
            }
        }
        return this.indexDirectory;
    }

    /**
     * Returns {@link IndexWriterConfig} object.
     * @return {@link IndexWriterConfig}
     */
    private IndexWriterConfig getIndexConfig() {
        if (this.indexConfig == null) {
            this.indexConfig = new IndexWriterConfig(this.getAnalyzer());
        }
        return this.indexConfig;
    }

    /**
     * Returns {@link Analyzer} object.
     * @return {@link Analyzer}
     */
    private Analyzer getAnalyzer() {
        if (this.analyzer == null) {
            this.analyzer = new SpecialCharAnalyzer();
        }
        return this.analyzer;
    }

    @Override
    public Set<CompositeDescription> search(HashMap<String, List<String>> queryParams, boolean fullConcept) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'search'");
    }

    @Override
    public Concept search(String conceptId)
            throws ParseException, IOException, org.apache.lucene.queryparser.classic.ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'search'");
    }

    @Override
    public List<Relationship> getRelationships(String conceptId, String relationshipType, EnumDirection directions) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getRelationships'");
    }

    @Override
    public Set<Concept> searchTC(String id) throws ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'searchTC'");
    }

    @Override
    public List<Relationship> getConcepts(HashMap<String, List<String>> queryParams)
            throws IOException, ParseException, org.apache.lucene.queryparser.classic.ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getConcepts'");
    }

    @Override
    public Set<String> getSemanticTags() throws IOException, org.apache.lucene.queryparser.classic.ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getSemanticTags'");
    }

    @Override
    public List<String> getRelationType() throws IOException, org.apache.lucene.queryparser.classic.ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getRelationType'");
    }

    @Override
    public SimpleMapRefsetResult searchSimpleMap(String refsetComponentId)
            throws ParseException, IOException, org.apache.lucene.queryparser.classic.ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'searchSimpleMap'");
    }

    @Override
    public Set<RelationshipTC> getIsARelationshipsWithCount(String conceptId) {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getIsARelationshipsWithCount'");
    }

    @Override
    public Set<Refset> getAllRefset()
            throws org.apache.lucene.queryparser.classic.ParseException, IOException, ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getAllRefset'");
    }

    @Override
    public Set<Refset> getAllExtension()
            throws org.apache.lucene.queryparser.classic.ParseException, IOException, ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getAllExtension'");
    }

    @Override
    public Set<Concept> getRefsetMember(String refsetId)
            throws org.apache.lucene.queryparser.classic.ParseException, IOException, ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getRefsetMember'");
    }

    @Override
    public Set<Concept> getExtensionMember(String moduleId, int pageNumber)
            throws org.apache.lucene.queryparser.classic.ParseException, IOException, ParseException {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getExtensionMember'");
    }
    
}
