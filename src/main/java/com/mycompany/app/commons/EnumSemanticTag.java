package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumSemanticTag {

    ALL("all"),
    SITUATION_WITH_EXPLICIT_CONTENT("situation");

    private static final Map<String, EnumSemanticTag> lookup = new ConcurrentHashMap<>();

    static {
        for(EnumSemanticTag d : EnumSemanticTag.values()) {
            lookup.put(d.getValue(), d);
        }
    }

    private EnumSemanticTag(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static EnumSemanticTag getEnum(String value) {
        return lookup.get(value);
    }
}
