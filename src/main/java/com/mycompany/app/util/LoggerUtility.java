package com.mycompany.app.util;

import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import com.mycompany.app.commons.Constants;

public class LoggerUtility {
    
    public static final Marker CONSOLE = MarkerManager.getMarker("Console");
    public static final Marker Log = MarkerManager.getMarker("Log");
    public static final Marker ConsoleWithLog = MarkerManager.getMarker("ConsoleWithLog");
    public static final Marker ConsoleWithException = MarkerManager.getMarker("ConsoleWithException");
    public static final Marker Exception = MarkerManager.getMarker("Exception");
    public static final Marker DISBException = MarkerManager.getMarker("DISBException");
    public static final Marker MapICDException = MarkerManager.getMarker("MapICDException");
    public static final Marker ConsoleWithMapICDException = MarkerManager.getMarker("ConsoleWithMapICDException");

    public static String createErrorMessage(Exception exception) {
        StackTraceElement[] arrStackTraceElement = exception.getStackTrace();
        String strMessage = "";
        try {
            strMessage = exception.getClass() + Constants.NEW_LINE + Constants.CLASS_NAME + arrStackTraceElement[0].getClassName() + Constants.NEW_LINE + Constants.METHOD_NAME + arrStackTraceElement[0].getMethodName() + Constants.NEW_LINE + Constants.LINE_NUMBER + arrStackTraceElement[0].getLineNumber() + Constants.NEW_LINE + Constants.MESSAGE_DESCRIPTION + exception.getMessage();
        } catch (Exception e) {
            return strMessage;
        }
        return strMessage;
    }
}
