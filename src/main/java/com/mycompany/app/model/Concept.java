package com.mycompany.app.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mycompany.app.commons.EnumDefStatus;
import com.mycompany.app.commons.EnumDirection;
import com.mycompany.app.commons.EnumLangRefset;
import com.mycompany.app.commons.EnumSemanticTag;
import com.mycompany.app.db.QueryManager;
import com.mycompany.app.util.LoggerUtility;
import com.mycompany.app.util.SemanticTagBuilder;

/**
 * {@link Concept} is a clinical idea associated with a unique identifier . A {@link Concept} consists
 * of an identifier, a fully specified name, a semantic tag of type {@link EnumSemanticTag} to which a
 * concept belongs and a {@link List} of {@link Description}s which are the synonyms describing the {@link Concept}.
 *
 */
public class Concept extends Component implements Serializable, Comparable<Concept> {

    private static final Logger logger = LogManager.getLogger(Concept.class);
    private static final long serialVersionUID = 1L;

    /**
	 * An identifier that tells whether the concept is fully Defined or primitive.
	 */
    private EnumDefStatus definitionStatusId;

    /**
	 * A human readable name for the concept.
	 */
    private String fullySpecifiedName;

    /**
	 * The semantic tag type of this concept
	 */
    private String semanticTag;

    /**
	 * All available descriptions of the this concept
	 */
    private List<Description> descriptions;
    
    /*
	 * prefferd name of term in us dialect
	 *
	 */
    private String preferredName;
    
    /**
     * Default Constructor
     */
    protected Concept() {
    }

    /**
     * Parameterized Constructor
     * @param id            represents Concept Id
     * 
     */
    protected Concept(String id) throws SQLException, ParseException {
        loadConcept(id);
    }

    /**
	 * Parameterized constructor
	 * @param id
	 * 			 		 represents the Concept Id.
	 * @param defStatusId
	 * 					 represents the definition status Id of that particular concept.
	 * @param effTime
	 * 					 represents the effective time.
	 * @param status
	 * 					 represents the status of that concept (1 for active and 0 for inactive).
	 * @param modId
	 * 					 represents the module Id associated with the concept.
	 * @throws SQLException
	 * @throws ParseException
	 */
    protected Concept(EnumDefStatus definitionStatusId, String id, Date effectiveTime, Byte activeStatus, String moduleId) throws SQLException, ParseException {
        super(id, effectiveTime, activeStatus, moduleId);
        this.definitionStatusId = definitionStatusId;
        loadConcept(id);
    }

    /**
	 * Loads the attribute values for a particular concept.
	 * @param id
	 * 			 represents the SNOMED CT Concept Identifier.
	 * @param qm
	 * 			 represents object of {@link QueryManager} class.
	 * @throws SQLException
	 * @throws ParseException
	 */
    private void loadConcept(String id) throws SQLException, ParseException {
        QueryManager qm = new QueryManager();
        qm.populateConcept(id, this);
    }

    /**
	 * Returns the {@link Concept} identifier.
	 * @return id of that particular Concept.
	 */
    public String getId() {
        return super.getId();
    }

    /**
	 * Sets the Concept identifier.
	 * @param id
	 * 			 represents Concept identifier.
	 */
    public void setId(String id) {
        super.setId(id);
    }

    /**
	 * Returns the Concept FSN.
	 * @return FSN (Fully Specified Name) of that concept.
	 */
    public String getFullySpecifiedName() {
        return fullySpecifiedName;
    }

    /**
	 * Sets the fully specified name associated with the
	 * Concept.
	 * @param fullySpecifiedName            represents FSN of a concept.
	 */
    public void setFullySpecifiedName(String fullySpecifiedName) {
        this.fullySpecifiedName = fullySpecifiedName;
    }

    
    /**
	 * Returns the definition status.
	 * @return definitionStatus Id of that concept.
	 */
    public String getDefinitionStatusId() {
        return definitionStatusId.getValue();
    }

    /**
	 * Sets the definition status Id of the Concept.
	 * @param definitionStatusId           represents definition status Id of a concept.
	 *
	 */
    public void setDefinitionStatusId(EnumDefStatus definitionStatusId) {
        this.definitionStatusId = definitionStatusId;
    }

    /**
	 * Returns the active status of the Concept.
	 * @return activeStatus of that concept.
	 */
    public Byte getActiveStatus() {
        return super.getActiveStatus();
    }

    /**
	 * Sets the active status of the Concept.
	 * @param activeStatus			refers to the active status of the Concept.
	 */
    public void setActiveStatus(Byte activeStatus) {
        super.setActiveStatus(activeStatus);
    }

    /**
	 * Returns the module Id.
	 * @return moduleId of that concept.
	 */
    public String getModuleId() {
        return super.getModuleId();
    }

    /**
	 * Sets the module Id associated with the Concept.
	 * @param modId 	 represents Module Id of the concept
	 * sets moduleId of a concept.
	 */
    public void setModuleId(String moduleId) {
        super.setModuleId(moduleId);
    }

    /**
    * 
    * @return effective time of that component
    */
    public Date getEffectiveTime() {
        return super.getEffectiveTime();
    }

    /**
    * sets the effective time for the component
    * @param effectiveTime - represents effective time
    */
    public void setEffectiveTime(Date effectiveTime) {
        super.setEffectiveTime(effectiveTime);;
    }

    /**
	 * Sets the list of {@link Description} corresponding to the SNOMED CT concept.
	 * @param descriptions		List of {@link Description}.
	 */
    public void setDescriptions(List<Description> descriptions) {
        this.descriptions = descriptions;
    }

    public List<Description> getDescriptions() throws SQLException, ParseException {
        if(null == this.descriptions) {
            QueryManager qm = new QueryManager();
            descriptions = qm.getDescriptions(this.id);
        }
        return this.descriptions;
    }

    /**
	 *  @return {@link String} value of preferred name
	 */
    public String getPreferredName() {
        return preferredName;
    }

    public String setPreferredName(EnumLangRefset langRefset) throws SQLException {
        if(null == this.preferredName) {
            QueryManager qm = new QueryManager();
            this.preferredName = qm.getPreferredName(this.id, langRefset);
        }
        return this.preferredName;
    }

    public void setPrefferedName(String preferredName) {
        this.preferredName = preferredName;
    }

    public Set<Concept> getRelations(String relationshipType, EnumDirection direction) {
        QueryManager qm = new QueryManager();
        List<Relationship> listRelationships = null;
        Set<Concept> relConcepts = new HashSet<Concept>();

        try {
            if(direction == EnumDirection.DESTINATION) {
                listRelationships = qm.getRelationships(this.id, relationshipType, direction);
                for(Relationship relationship : listRelationships) {
                    relConcepts.add(relationship.getSourceConcept());
                } 
            } else {
                listRelationships = qm.getRelationships(this.id, relationshipType, direction);
                for(Relationship relationship : listRelationships) {
                    relConcepts.add(relationship.getDestinationConcept());
                }
            }
        } catch(Exception ex) {
            logger.error(LoggerUtility.Exception, LoggerUtility.createErrorMessage(ex));
        }
        return relConcepts;
    }

    public String getSemanticTag() {
        if(this.semanticTag == null) {
            String semanticTag = SemanticTagBuilder.buildSemanticTagFromString(this.fullySpecifiedName);
            if(semanticTag == null) {
                this.semanticTag = EnumSemanticTag.SITUATION_WITH_EXPLICIT_CONTENT.getValue();
            } else {
                throw new IllegalArgumentException("semantic tag is null");
            }
        }
        return this.semanticTag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activeStatus == null) ? 0 : activeStatus.hashCode());
        result = prime * result + ((descriptions == null) ? 0 : descriptions.hashCode());
        result = prime * result + ((fullySpecifiedName == null) ? 0 : fullySpecifiedName.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((semanticTag == null) ? 0 : semanticTag.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Concept concept = (Concept) obj;
        if(activeStatus != concept.activeStatus)
            return false;
        if(descriptions == null && concept.descriptions != null)
            return false;
        if(!descriptions.equals(concept.descriptions))
            return false;
        if(fullySpecifiedName == null && concept.fullySpecifiedName != null)
            return false;
        if(!fullySpecifiedName.equals(concept.fullySpecifiedName))
            return false;
        if(id == null && concept.id != null)
            return false;
        if(!id.equals(concept.id))
            return false;
        if(!semanticTag.equals(concept.semanticTag))
            return false;
        return true;
    }

    @Override
    public int compareTo(Concept o) {
        return this.compareTo(o);
    }
    
}
