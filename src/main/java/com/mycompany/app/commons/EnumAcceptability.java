package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumAcceptability {
    
    ALL("all"),
	PREFERRED("preferred"),
	PREFERRED_EXCLUDING_FSN("preferredexcludingfsn"),
	SYNONYMS("synonyms"),
	ACCEPTABLE("acceptable");
	
	private String value;
	
	private static final Map<String, EnumAcceptability> lookup = new ConcurrentHashMap<String, EnumAcceptability>();
    
    static {
        for (EnumAcceptability d : EnumAcceptability.values())
            lookup.put(d.getValue(), d);
    }
	
	private EnumAcceptability(String value) {
		this.value = value;
	}
	
	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
	 
	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumAcceptability} enumeration for given string value
	 */
	public static EnumAcceptability getEnum(String val) {
		return lookup.get(val);
	}

}
