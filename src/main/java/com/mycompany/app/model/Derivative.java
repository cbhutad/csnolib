package com.mycompany.app.model;

public abstract class Derivative extends VersionedComponent {

    /**
     * Uniquely identifies a derivative or reference set member.
    */
    protected String id;

    /**
	 * Returns the derivative Id.
	 * @return id of Derivative.
	 */
    public String getId() {
        return id;
    }

    /**
     * Sets the derivative id.
     * @param id
     *                  identifier to uniquely identify a derivative.
     */
    public void setId(String id) {
        this.id = id;
    }

}
