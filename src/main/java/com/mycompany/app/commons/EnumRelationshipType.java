package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumRelationshipType {
    
    ALL("all",""),
    ATTRIBUTE("attribute","410662002"),
	IS_A("is a", "116680003");

    private static final Map<String, EnumRelationshipType> lookupForValue = new ConcurrentHashMap<>();
    private static final Map<String, EnumRelationshipType> lookupForId = new ConcurrentHashMap<>();

    static {
        for(EnumRelationshipType d : EnumRelationshipType.values()) {
            lookupForValue.put(d.getValue(),d);
            lookupForId.put(d.getId(), d);
        }
    }
    
    private EnumRelationshipType(String value, String id) {
        this.value = value;
        this.id = id;
    }

    private String value;
    private String id;

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public static EnumRelationshipType getEnumForValue(String value) {
        return lookupForValue.get(value);
    }

    public static EnumRelationshipType getEnumForId(String id) {
        return lookupForId.get(id);
    }
}
