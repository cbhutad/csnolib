package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumState {
    
    ACTIVE("active"),
    INACTIVE("inactive"),
    BOTH("both");

    private static final Map<String, EnumState> lookup = new ConcurrentHashMap<String, EnumState>();

    static {
        for (EnumState d : EnumState.values()) {
            lookup.put(d.getValue(), d);
        }
    }

    private String value;

    private EnumState(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static EnumState getEnum(String value) {
        return lookup.get(value);
    }
}
