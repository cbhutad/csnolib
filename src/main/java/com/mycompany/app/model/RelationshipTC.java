package com.mycompany.app.model;

/** 
 *This class is used for finding descendant
 */ 
public class RelationshipTC {
    
    /**
	 * represent parent id of the relationships_tc.
	 */
	private String parentId;
	/**
	 * Child concept of the relationships_tc.
	 */
	private Concept childConcept;
	/**
	 * Count of all descendant Children concept of the relationships_tc.
	 */
	private int countOfChildrens ;
	/**
	 * Is Immediate Children concept of the relationships_tc.
	 */
	private int isImmediateChildren ;

    /**
     * Default Constructor
     */
    public RelationshipTC() {
    }

    /**
     * Parameterized Constructor
     * @param parentId
     * @param childConcept
     */
    public RelationshipTC(String parentId, Concept childConcept) {
        this.parentId = parentId;
        this.childConcept = childConcept;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Concept getChildConcept() {
        return childConcept;
    }

    public void setChildConcept(Concept childConcept) {
        this.childConcept = childConcept;
    }

    public int getCountOfChildrens() {
        return countOfChildrens;
    }

    public void setCountOfChildrens(int countOfChildrens) {
        this.countOfChildrens = countOfChildrens;
    }

    public int getIsImmediateChildren() {
        return isImmediateChildren;
    }

    public void setIsImmediateChildren(int isImmediateChildren) {
        this.isImmediateChildren = isImmediateChildren;
    }

    
}
