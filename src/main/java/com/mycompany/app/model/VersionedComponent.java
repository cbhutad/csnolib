package com.mycompany.app.model;

import java.util.Date;

/**
 * VersionedComponent is the super class for all the Components
 * Defined as an abstract class as it need not be instantiated
 */
public abstract class VersionedComponent {

    /**
     * Represents effective time of the component 
    */
    protected Date effectiveTime;

    /**
     * A status of the concept (active=1 and inactive =0) 
    */
    protected Byte activeStatus;

    /**
     * Module id of the component
    */
    protected String moduleId;

    /**
     * Default constructor
    */
    protected VersionedComponent() {	 
    }

    /**
     * Parameterized constructor
    * @param effectiveTime
    * 			 represents effective time of the SNOMED CT component
    * @param activeStatus
    * 			 represents active status of the component (active or inactive)
    * @param moduleId
    * 			 represents module Id of the component
    */
    protected VersionedComponent(Date effectiveTime, Byte activeStatus, String moduleId) {
        this.effectiveTime = effectiveTime;
        this.activeStatus = activeStatus;
        this.moduleId = moduleId;
    }

    /**
    * 
    * @return effective time of that component
    */
    public Date getEffectiveTime() {
        return effectiveTime;
    }

    /**
    * sets the effective time for the component
    * @param effectiveTime - represents effective time
    */
    public void setEffectiveTime(Date effectiveTime) {
        this.effectiveTime = effectiveTime;
    }

    /**
    * Retrieves active status of a component.
    * @return activeStatus of a component
    */
    public Byte getActiveStatus() {
        return activeStatus;
    }

    /**
    * sets the active status attribute of a component
    * @param activeStatus
    * 			 represents active Status of the component
    */
    public void setActiveStatus(Byte activeStatus) {
        this.activeStatus = activeStatus;
    }

    /**
    * 
    * @return module Id of a component
    */
    public String getModuleId() {
        return moduleId;
    }

    /**
    * sets the module Id attribute of a component object
    * @param moduleId
    * 			 represents module Id of a component
    */
    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }    
    
}
