package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumDirection {

    SOURCE("source"),
	DESTINATION("destination");
	
	private static final Map<String, EnumDirection> lookup = new ConcurrentHashMap<String, EnumDirection>();
    
    static {
        for (EnumDirection d : EnumDirection.values())
            lookup.put(d.getValue(), d);
    }
    
    private String value;
	
	private EnumDirection( String value) {
		this.value = value;
	}
	
	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
 
	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumDirection} enumeration for given string value
	 */
	public static EnumDirection getEnum(String val) {
		return lookup.get(val);
	}

    
}
