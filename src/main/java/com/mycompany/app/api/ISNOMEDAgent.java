package com.mycompany.app.api;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import com.mycompany.app.commons.EnumAcceptability;
import com.mycompany.app.commons.EnumDirection;
import com.mycompany.app.commons.EnumLangRefset;
import com.mycompany.app.commons.EnumState;
import com.mycompany.app.model.CompositeDescription;
import com.mycompany.app.model.Concept;
import com.mycompany.app.model.ConceptDetails;
import com.mycompany.app.model.Refset;
import com.mycompany.app.model.Relationship;
import com.mycompany.app.model.RelationshipTC;
import com.mycompany.app.model.SimpleMapRefsetResult;

/**
 * Library interface for application. It provides the basic methods to query the
 * SNOMED CT repository for finding suggestions, searching and retrieving
 * {@link Concept} and corresponding {@link Description} for known concept identifiers.
 *
 * @see Concept
 * @see Description
 * @see EnumSemnaticTag
 * @see EnumState
 * @see EnumAcceptability
 */

public interface ISNOMEDAgent extends Serializable {

	/**
	 * Returns the list of descriptions including Fully Specified names (FSN).
	 * @param conceptId SNOMED CT concept identifier
	 * @param enumLangRefset {@link EnumLangRefset} specifies English language reference set for US or UK.
	 * @return List of {@link CompositeDescription} for given concept id.
	 */
	public List<CompositeDescription> getAllDescriptions(String conceptId, EnumLangRefset enumLangRefset);

	/**
	 * Returns all the relationships for a given concept id.
	 * @param conceptId SNOMED CT concept identifier.
	 * @throws SQLException if error occurs while accessing database.
	 * @return List of {@link Relationship} corresponding to the given concept id.
	 */
	public List<Relationship> getAllRelationships(String conceptId) throws SQLException;

	/**
	 * Returns Parent Concepts for a given concept Id.
	 * @param ConceptId SNOMED CT concept identifier.
	 * @return Set of parent concepts for given concept id.
	 */
	public Set<Concept> getParents(String conceptId) throws SQLException;

	/**
	 * Returns concept details associated for a given concept identifier (corresponding descriptions and relationships are also listed)
	 * @param id {@link String} identifier of the SNOMED CT {@link Concept}.
	 * @param enumLangRefset {@link EnumLangRefset} Specifies English language reference set for US or UK.
	 * @return {@link ConceptDetails} that contains the details corresponding to a particular SNOMED CT Concept
	 * (including descriptions and relationships related to that particular concept).
	 */
	public ConceptDetails getConceptDetails(String id, EnumLangRefset enumLangRefset);

	/**
	 * Searches given conceptId in the SNOMED CT repository<br>
	 * @param conceptId String it will search on the basis of given conceptId and also use for checking
	 * the entered conceptId is valid or not<br>
	 * @return Object of {@link Concept} respected to given conceptId
	 *
	 */
	public Concept search(final String conceptId);

	/**
	 * Searches given term/text or concept id in SNOMED CT repository. Result includes active as well as inactive terms.
	 *  @param matchTerm {@link String} term/text or Concept ID to be searched.
	 *  @param groupByConcept boolean value, if true, then method will return set of {@link CompositeDescription} which will contain one
	 *  			description from one concept.
	 *  @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 *  @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link CompositeDescription} matching the given term/text or Concept ID.
	 */
	public Set<CompositeDescription> search(String matchTerm, boolean groupByConcept, String refsetId, List<String> parentIdList, boolean excludeConcept);

	/**
	 * Searches limited best matching terms for given term/text or concept iD in SNOMED CT repository.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param groupByConcept boolean value, if true, then method will return set of {@link CompositeDescription} which will contain one
	 *  			description from one concept.
	 *  @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 *  @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link CompositeDescription} matching the given term/text or Concept ID.
	 */
	public Set<CompositeDescription> search(String matchTerm ,int returnLimit, boolean groupByConcept,String refsetId, List<String> parentIdList,boolean excludeConcept);

	/**
	 * Searches given term/text or concept id in the SNOMED CT repository based on provided component state (active, inactive, both).
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param enumState {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 		  inactive - It will return only inactive Components.<br>
	 * 		  both - It will return both active and inactive Components.<br>
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param groupByConcept boolean value, if true, then method will return set of {@link CompositeDescription} which will contain one
	 *  			description from one concept.
	 *  @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 *  @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link CompositeDescription} matching the given term/text or Concept ID.
	 */
	public Set<CompositeDescription> search(String matchTerm,EnumState enumState,int returnLimit, boolean groupByConcept,String refsetId, List<String> parentIdList,boolean excludeConcept);

	/**
	 * Searches given term/text or concept id in the SNOMED CT repository based on provided component state, semantic tag.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param enumState {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 	 	  inactive - It will return only inactive Components.<br>
	 * 		  both - It will return both active and inactive Components.<br>
	 * @param enumSemanticTagList
	 *            {@link EnumSemanticTag} Refers to the Semantic Tag in which SNOMED CT term/text or concept id will be searched. <br>
	 *            If enumSemanticTag is,<br>
	 *            all - It will search SNOMED CT term in all the Semantic Tags.<br>
	 *            For other possible values please refer {@link EnumSemanticTag}.<br>
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param groupByConcept boolean value, if true, then method will return set of {@link CompositeDescription} which will contain one
	 *  			description from one concept.
	 *  @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 *  @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
* @return Set of {@link CompositeDescription} matching the given term/text or Concept ID.
	 */
	public Set<CompositeDescription> search(String matchTerm,EnumState enumState,List<String> semanticTagList, int returnLimit, boolean groupByConcept, String refsetId, List<String> parentIdList,boolean excludeConcept);

	/**
	 * Searches given term/text or concept id in the SNOMED CT repository based on provided component state, semantic tag, acceptability.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param enumState {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 		  inactive - It will return only inactive Components.<br>
	 * 		  both - It will return both active and inactive Components.<br>
	 * @param enumSemanticTagList
	 *            {@link EnumSemanticTag} Refers to the Semantic Tag in which SNOMED CT term/text or concept id will be searched. <br>
	 *            If enumSemanticTag is,<br>
	 *            all - It will search SNOMED CT term in all the Semantic Tags.<br>
	 *            For other possible values please refer {@link EnumSemanticTag}.<br>
	 * @param enumAcceptability
	 *            {@link EnumAcceptability} Represents the type of description i.e. whether the search results contains preferred terms or
	 *            preferred terms other than FSN or only acceptable terms and so on.<br>
	 *            If enumAcceptability is, <br>
	 *            preferred - refers to all preferred description for a concept (including FSN). <br>
	 *  		  preferredexcludingfsn - refers to SNOMEDCT description for preferred terms excluding FSN.<br>
	 *  		  synonyms - refers to all description for a concept (excluding FSN).<br>
	 *  		  acceptable- refers to all description for a concept excluding preferred.<br>
	 *  		  all - refers to all description for a concept (both preferred and acceptable).<br>
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param groupByConcept boolean value, if true, then method will return set of {@link CompositeDescription} which will contain one
	 * 			description from one concept.
	 * @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 * @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
* @return Set of {@link CompositeDescription} matching the given term/text or Concept ID.
	 */
	public Set<CompositeDescription> search(String matchTerm,EnumState enumState,List<String> semanticTagList,EnumAcceptability enumAcceptability,int returnLimit, boolean groupByConcept,String refsetId, List<String> parentIdList,boolean excludeConcept);

	/**
	 * Searches given term/text or concept id in the SNOMED CT repository based on provided component state, semantic tag, acceptability.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param enumState {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 			  inactive - It will return only inactive Components.<br>
	 * 			  both - It will return both active and inactive Components.<br>
	 * @param enumSemanticTagList
	 *            {@link EnumSemanticTag} Refers to the Semantic Tag in which SNOMED CT term/text or concept id will be searched. <br>
	 *            If enumSemanticTag is,<br>
	 *            all - It will search SNOMED CT term in all the Semantic Tags.<br>
	 *            For other possible values please refer {@link EnumSemanticTag}.<br>
	 * @param enumAcceptability
	 *            {@link EnumAcceptability} Represents the type of description i.e. whether the search results contains preferred terms or
	 *            preferred terms other than FSN or only acceptable terms and so on.<br>
	 *            If enumAcceptability is, <br>
	 *            preferred - refers to all preferred description for a concept (including FSN). <br>
	 *  		  preferredexcludingfsn - refers to SNOMEDCT description for preferred terms excluding FSN.<br>
	 *  		  synonyms - refers to all description for a concept (excluding FSN).<br>
	 *  		  acceptable- refers to all description for a concept excluding preferred.<br>
	 *  		  all - refers to all description for a concept (both preferred and acceptable).<br>
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param groupByConcept boolean value, if true, then method will return set of {@link CompositeDescription} which will contain one
	 * 			description from one concept.
	 * @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 * @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param fullConcept boolean value, if true, then method will return all details for concept otherwise limited data like FSN,term and concpetid
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link CompositeDescription} matching the given term/text or Concept ID.
	 */
	public Set<CompositeDescription> search(String matchTerm,EnumState enumState,List<String> semanticTagList,EnumAcceptability enumAcceptability,int returnLimit, boolean groupByConcept,String refsetId, List<String> parentIdList,boolean fullConcept,boolean excludeConcept);



	/**
	 * Searches given term/text or concept id in the SNOMED CT repository based on provided component state (active, inactive, both).
	 * by the user.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param enumState {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 			  inactive - It will return only inactive Components.<br>
	 * 			  both - It will return both active and inactive Components.<br>
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param groupByConcept boolean value, if true, then method will return set of {@link CompositeDescription} which will contain one
	 *  			description from one concept.
	 *  @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 *  @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 *  @param fullConcept boolean value, if true, then method will return all details for concept otherwise limited data like FSN,term and concpetid
	 *  @param sematicTagList List<{@link String}> contains semantic tags list
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link CompositeDescription} matching the given term/text or Concept ID.
	 */
	public Set<CompositeDescription> search(String matchTerm, EnumState enumState,
			List<String> semanticTagList, int returnLimit, boolean groupByConcept,String refsetId, List<String> parentIdList,boolean fullConcept,boolean excludeConcept);

/**
	 * Returns most matching suggestions for given term/text or concept id in SNOMED CT repository.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched.
	 * @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 * @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link String} suggestions matching the given term/text or Concept ID.
	 */
	public Set<String> getSuggestions(String matchTerm,String refsetId,List<String> parentIdList,boolean excludeConcept);
	
	/**
	 * Returns limited best matching suggestions for given term/text or concept id in SNOMED CT repository.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 * @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link String} matching the given term/text or Concept ID.
	 */
	public Set<String> getSuggestions(String matchTerm,int returnLimit,String refsetId, List<String> parentIdList,boolean excludeConcept);
	
	/**
	 * Returns most matching suggestions for term/text or concept id in the SNOMED CT repository based on provided component state (active, inactive, both). 
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param enumState {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 			  inactive - It will return only inactive Components.<br>
	 * 			  both - It will return both active and inactive Components.<br>
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 * @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link String} matching the given term/text or Concept ID.
	 */
	public Set<String> getSuggestions(String matchTerm,EnumState enumState,int returnLimit,String refsetId, List<String> parentIdList,boolean excludeConcept);
		
	/**
	 * Returns most matching suggestions for given term/text or concept id in the SNOMED CT repository based on provided component state, semantic tag.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param enumState {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 			  inactive - It will return only inactive Components.<br>
	 * 			  both - It will return both active and inactive Components.<br>
	 * @param enumSemanticTagList
	 *            {@link EnumSemanticTag} Refers to the Semantic Tag in which SNOMED CT term/text or concept id will be searched. <br>
	 *            If enumSemanticTag is,<br>
	 *            all - It will search SNOMED CT term in all the Semantic Tags.<br>
	 *            For other possible values please refer {@link EnumSemanticTag}.<br>
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 * @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link String} matching the given term/text or Concept ID.
	 */
	public Set<String> getSuggestions(String matchTerm,EnumState enumState,List<String> semanticTagList ,int returnLimit,String refsetId, List<String> parentIdList,boolean excludeConcept);
	
	/**
	 * Returns most matching suggestions for given term/text or concept id in the SNOMED CT repository based on provided component state, semantic tag, acceptability.
	 * @param matchTerm {@link String} term/text or Concept ID to be searched in SNOMED CT repository.
	 * @param enumState {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 			  inactive - It will return only inactive Components.<br>
	 * 			  both - It will return both active and inactive Components.<br>
	 * @param enumSemanticTagList
	 *            {@link EnumSemanticTag} Refers to the Semantic Tag in which SNOMED CT term/text or concept id will be searched. <br>
	 *            If enumSemanticTag is,<br>
	 *            all - It will search SNOMED CT term in all the Semantic Tags.<br>
	 *            For other possible values please refer {@link EnumSemanticTag}.<br>
	 * @param enumAcceptability
	 *            {@link EnumAcceptability} Represents the type of description i.e. whether the search results contains preferred terms or 
	 *            preferred terms other than FSN, only acceptable terms or synonyms.<br>
	 *            If enumAcceptability is, <br>
	 *            preferred - refers to all preferred description for a concept (including FSN). <br>
	 *  		  preferredexcludingfsn - refers to SNOMEDCT description for preferred terms excluding FSN.<br>
	 *  		  synonyms - refers to all description for a concept (excluding FSN).<br>
	 *  		  acceptable- refers to all description for a concept excluding preferred.<br>
	 *  		  all - refers to all description for a concept (both preferred and acceptable).<br>
	 * @param returnLimit Maximum number of matching terms to be fetched.
	 * @param refsetId Identifier of Refset if search is to be done from a specific Refset and null if search is to be done from whole SNOMED CT Terminology.
	 * @param parentIdList List of Identifier of Concepts if search is to be done from it's descendants only and null if search is to be done from whole SNOMED CT Terminology.
	 * @param excludeConcept  give result for search term excluding the given concepts ID (parentID)
	 * @return Set of {@link String} matching the given term/text or Concept ID.
	 */
	public Set<String> getSuggestions(String matchTerm,EnumState enumState,List<String> semanticTagList,EnumAcceptability enumAcceptability,int returnLimit,String refsetId, List<String> parentIdList,boolean excludeConcept);

	/**
	 * Returns a {@link Set} of all Descendants {@link Concept}s for given concept identifier.
	 * @param id SNOMED CT concept identifier.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getAllDescendantConcepts(String id);

	/**
	 * Returns a {@link Set} of all Refset Members {@link Concept}s for given concept refset identifier.
	 * @param id SNOMED CT concept identifier.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getAllRefsetMembers(String id);

	/**
	 * Returns a {@link Set} of all Descendants {@link Concept}s for given concept identifier.	 *
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getAllRefsets();

	/**
	 * Retrieves the Semantic Tag for a given concept identifier.
	 * @param conceptId {@link String} value for Concept identifier for which hierarchy is to be identified.
	 * @param typeId type of the description.
	 * @param term the description for that concept.
	 * @return the hierarchy to which the concept belongs.
	 * @throws SQLException if error occurs while accessing database.
	 */
	public String getHierarchy(String conceptId, String typeId,
			String term) throws SQLException;

	/**
	 * Returns a SNOMED CT {@link Concept} associated for a given {@link Concept} identifier.
	 * @param id {@link String} identifier of the SNOMED CT {@link Concept}.
	 * @return {@link Concept} SNOMED CT concept
	 */
	public Concept getConcept(String id);

	/**
	 * Returns {@link List} of SNOMED CT {@link Concept} associated for a given {@link Concept} identifier.
	 * @param ids {@link List} {@link String} identifier of the SNOMED CT {@link Concept}.
	 * @return {@link List} {@link Concept} SNOMED CT concept
	 */
	public List<Concept> getConcept(List<String> ids);

	/**
	 *  Returns all concept associated for a given attribute name
	 * @param attributeName
	 * @return  {@link Relationship} that contains the list of Relationships corresponding to a particular SNOMED CT attribute value
	 */
	public List<Relationship> getConcepts(String attribute);

	/**
	 * Returns all concept associated for a given attribute name and semantic tags
	 * @param attributeName
	 * @param semanticTagList
	 * @return  {@link Relationship} that contains the list of Relationships corresponding to a particular SNOMED CT attribute value
	 */
	public List<Relationship> getConcepts(String attribute,List<String> semanticTagList);

	/**
	 * Returns all concept associated for a given attribute name and destination id (attribute value)
	 * @param attributeName
	 * @param destinationId
	 * @return  {@link Relationship} that contains the list of Relationships corresponding to a particular SNOMED CT attribute value
	 */
	public List<Relationship> getConcepts(String attribute,String destinationId);

	/**
	 * Returns all concept associated for a given attribute name, semantic tags and destination id (attribute value)
	 * @param attributeName
	 * @param semanticTagList
	 * @param destinationId
	 * @return  {@link Relationship} that contains the list of Relationships corresponding to a particular SNOMED CT attribute value
	 */
	public List<Relationship> getConcepts(String attribute,List<String> semanticTagList,String destinationId);

	/**
	 * Returns all concept associated for a given attribute name and other parameters
	 * @param attributeName
	 * @param semanticTagList
	 * @param destinationId
	 * @param subsumption
	 * @param parentIdList
	 * @param excludeConcept
	 * @return  {@link Relationship} that contains the list of Relationships corresponding to a particular SNOMED CT attribute value
	 */
	public List<Relationship> getConcepts(String attribute, List<String> semanticTagList, String destinationId,
            boolean subsumption, List<String> parentIdList, boolean excludeConcept);

	/**
	 * Searches given concept id in the SNOMED CT repository based on provided relationship type, direction.
	 * @param
	 *  conceptId - {@link String} Concept ID to be searched in SNOMED CT repository.<br>
	 * 	enumRelationshipType - {@link EnumRelationshipType} Type of relationship.<br>
	 *            If enumRelationshipType is,<br>
	 *  		  is a - It will return only "is a" relation type Components.<br>
	 * 			  all - It will return only "all" relation type Components.<br>
	 * 			  finding site - It will return only "finding site" relation type Components.<br>
	 *  directions- {@link EnumDirection} Relationship direction i.e whether the specified concept Id is the source or destination
	 * 				Possible values for directions is,<br>
	 * 				source - concept Id is a source<br>
	 * 				destination - concept Id is a destination<br>
	 *  @return Set of {@link Concept} matching the given parameter.<br>
	 * @throws IOException
	 */
	public Set<Concept> getRelations(String conceptId,String relationshipType, EnumDirection direction);

	/**
	 * The method will configure properties by values provided in parameters
	 * @param dataDir location for database and lucene index.
	 * @param errorLogDir location for logs.
	 * @param searchReturnlimit maximum number record from which search should be perform example: 10000
	 * @param suggestReturnlimit maximum  number of record from which suggest should be perform example: 500
	 * @param topSuggestReturnlimit maximum number of top suggestions that would be return in suggestion list example: 50
	 */
	public void init(String dataDir, String errorLogDir, Integer searchReturnlimit , Integer suggestReturnlimit, Integer topSuggestReturnlimit);

	/**
	 * The method will configure the properties to default values
	 * */
	public void init();

	/**
	 * It will return all semantic tags based on provided SNOMED CT RF2
	 * @return Set<{@link String}> Set of {@link String} containing semantic tags
	 * @throws ParseException
	 */
	public Set<String> getSemanticTags();

	/**
	 * It will return all relationship type name based on provided SNOMED CT RF2
	 * @return List<{@link String}> List of {@link String} containing relationship type name
	 * @throws ParseException
	 */
	public List<String> getRelationShipType();

	/**
	 * This method used for retrieving language refset values from LangRefsetEnum
	 * @return List of List {@link String} containing language refset values in it
	 */
	public List<List<String>> getLangRefset();

	/**
	 * Searches for the immediate children descriptions.
 	 *@param conceptId
 	 *				Concept Id whose immediate children to search
	 * @return Set of {@link RelationshipTC} matching the given Concept ID.<br>
	 * @throws IOException if any error occurs while processing.
	 * @throws java.text.ParseException
	 */
	public Set<RelationshipTC> getIsARelationshipsWithCount(String conceptId);

	/**
	 * Returns a {@link Set} of {@link Refset}s for given concept identifier.	 *
	 * @return Set of {@link Refset}s.
	 */

	public Set<Refset> getAllRefset();

	/**
	 * Returns a {@link Set} of all Extensions {@link Refset}s.	 *
	 * @return Set of {@link Refset}s.
	 */

	public Set<Refset> getAllExtension();

	/**
	 * Returns a {@link Set} of all Refset Members {@link Concept}s for given concept refset identifier.
	 * @param id SNOMED CT concept identifier.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getRefsetMember(String string);
	/**
	 * Returns a {@link Set} of all Extension Members {@link Concept}s for given concept extension identifier.
	 * @param id SNOMED CT concept identifier.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getExtensionMember(String string, int pageNumber);
	/**
	 * Returns a {@link Set} of all Ancestors {@link Concept}s for given concept identifier.	 *
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getAllAncestorConcepts(String id);

	/**
	 * This method creates SNOMED CT database
	 * @param snomedIntDirPath Directory path for SNOMED CT International Release directory path example: G:\SnomedCT_InternationalRF2_PRODUCTION_20170731T150000Z\Snapshot
	 * @param overridedb will override database and index if true. It should be false after database creation process
	 */
	public boolean createSNOMEDdb(String snomedIntDirPath, boolean overridedb);
	/**
	 * This method creates SNOMED CT Refset database
	 * @param snomedIntDirPath Directory path for SNOMED CT International Release directory path example: G:\SnomedCT_InternationalRF2_PRODUCTION_20170731T150000Z\Snapshot
	 * @param overridedb will override database and index if true. It should be false after database creation process
	 */
	public boolean createSNOMEDRefsetdb(String snomedIntDirPath, boolean overridedb);


	/**
	 * This method creates database for given refsets
	 * @param refsetFilePath file path for refset example: G:/SnomedCT_GPFPICPC2_PRODUCTION_20171018T120000Z/Snapshot/Refset/Content/der2_Refset_SimpleSnapshot_INT_20170731.txt
	 * @param overridedb will override database and index if true. It should be false after database creation process
	 */
	public boolean createRefsetdb(String refsetFilePath, boolean overridedb);

	/**
	 * This method creates SNOMED CT extension database
	 * @param snomedExtDirPath Directory path for SNOMED CT extension Release directory path example: G:\SnomedCT_InternationalRF2_PRODUCTION_20170731T150000Z\Snapshot
	 * @param overridedb will override database and index if true. It should be false after database creation process
	 */
	public boolean createExtensiondb(String snomedExtDirPath, boolean overridedb);

	/**
	 * This method creates simple map  database
	 * @param snomedIntDirPath Directory path for SNOMED CT International Release directory path example: G:\SnomedCT_InternationalRF2_PRODUCTION_20170731T150000Z\Snapshot
	 * @param overridedb will override database and index if true. It should be false after database creation process
	 */
	public boolean createSimpleMapdb(String snomedIntDirPath, boolean overridedb);

	/**
	 * Searches the simpleMapTarget of refsetComponentId provided .
	 * @param queryParams
	 * @return  SimpleMapRefsetResult mapped to the given Refset Component ID.
	 * @throws IOException
	 * @throws ParseException
	 * @throws org.apache.lucene.queryparser.classic.ParseException
	 */
	public SimpleMapRefsetResult searchSimpleMap(String refsetComponentId );

}
