package com.mycompany.app.model;

import java.util.Date;

import com.mycompany.app.commons.EnumCharacteristicType;
import com.mycompany.app.commons.EnumModifier;

public class Relationship extends Component {
    
    /**
	 * Source concept id of the relationship.
	 */
    private Concept sourceConcept;

    /**
	 * Destination concept of the relationship.
	 */
    private Concept destinationConcept;

    /**
	 * Integer value that groups together the relationships that are logically associated.
	 */
    private int relationshipGroup;

    /**
	 * Type of relationship.
	 */
    private Concept type;

    /**
	 * Characteristic Type of the relationship.
	 */
    private EnumCharacteristicType characteristicTypeId;

    /**
	 * Id that refers to the modifier type (possible values for modifier- SOME, ALL).
	 */
    private EnumModifier modifierId;

    private boolean isConcreteRelationship = false;

    /**
	 * Default constructor.
	 */
    protected Relationship() {
    }

    protected Relationship(String id, Date effectiveTime, Byte activeStatus, String moduleId, Concept sourceConcept, Concept destinationConcept, int relationshipGroup, Concept type, EnumCharacteristicType characteristicTypeId, EnumModifier modifierId, boolean isConcreteRelationship) {
        super(id, effectiveTime, activeStatus, moduleId);
        this.sourceConcept = sourceConcept;
        this.destinationConcept = destinationConcept;
        this.relationshipGroup = relationshipGroup;
        this.characteristicTypeId = characteristicTypeId;
        this.modifierId = modifierId;
        this.isConcreteRelationship = isConcreteRelationship;
    }

    /**
	 * Sets the Concept identifier.
	 * @param id
	 * 			 represents Concept identifier.
	 */
    public void setId(String id) {
        super.setId(id);
    }

    /**
	 * Returns the active status of the Concept.
	 * @return activeStatus of that concept.
	 */
    public Byte getActiveStatus() {
        return super.getActiveStatus();
    }

    /**
	 * Sets the active status of the Concept.
	 * @param activeStatus			refers to the active status of the Concept.
	 */
    public void setActiveStatus(Byte activeStatus) {
        super.setActiveStatus(activeStatus);
    }

    /**
	 * Returns the module Id.
	 * @return moduleId of that concept.
	 */
    public String getModuleId() {
        return super.getModuleId();
    }

    /**
	 * Sets the module Id associated with the Concept.
	 * @param moduleId 	 represents Module Id of the concept
	 * sets moduleId of a concept.
	 */
    public void setModuleId(String moduleId) {
        super.setModuleId(moduleId);
    }

    /**
    * 
    * @return effective time of that component
    */
    public Date getEffectiveTime() {
        return super.getEffectiveTime();
    }

    /**
    * sets the effective time for the component
    * @param effectiveTime - represents effective time
    */
    public void setEffectiveTime(Date effectiveTime) {
        super.setEffectiveTime(effectiveTime);;
    }

    /**
	 * Returns the source Concept Id.
	 * @return the identifier of the source {@link Concept} of this relationship.
	 */
    public Concept getSourceConcept() {
        return sourceConcept;
    }

    /**
	 * Sets the source Concept Id.
	 * @param sourceConcept
	 * 						 Id of the source concept.
	 */
    public void setSourceConcept(Concept sourceConcept) {
        this.sourceConcept = sourceConcept;
    }

    /**
	 * Returns the destination Concept Id.
	 * @return the destination concept of this relationship.
	 */
    public Concept getDestinationConcept() {
        return destinationConcept;
    }

    /**
	 * Sets the destination Concept Id.
	 * @param destinationConcept
	 * 					 destination Concept.
	 */
    public void setDestinationConcept(Concept destinationConcept) {
        this.destinationConcept = destinationConcept;
    }

    /**
	 * Returns the relationship group.
	 * @return relationship group to which the particular {@link Relationship} belongs.
	 */
    public int getRelationshipGroup() {
        return relationshipGroup;
    }

    /**
	 * Sets the relationship group.
	 * @param relationshipGroup
	 * 						 integer value that represents a group of 
	 * relationships logically associated with one another.
	 */
    public void setRelationshipGroup(int relationshipGroup) {
        this.relationshipGroup = relationshipGroup;
    }

    /**
	 * 
	 * @return {@link EnumRelationshipType} the type of this relationship.
	 */
    public Concept getType() {
        return type;
    }

    /**
	 * Sets the type of the {@link Relationship}.
	 * @param type
	 * 			 represents the Relationship type.
	 */
    public void setType(Concept type) {
        this.type = type;
    }

    /**
	 * Returns the characteristic type.
	 * @return characteristic type Id associated with the {@link Relationship}.
	 */
    public EnumCharacteristicType getCharacteristicTypeId() {
        return characteristicTypeId;
    }

    /**
	 * Sets the characteristic type.
	 * @param characteristicTypeId
	 * 							specifies the characteristic type.
	 */
    public void setCharacteristicTypeId(EnumCharacteristicType characteristicTypeId) {
        this.characteristicTypeId = characteristicTypeId;
    }

    /**
	 * Returns the modifier Id.
	 * @return modifier Id.
	 */
    public EnumModifier getModifierId() {
        return modifierId;
    }

    /**
	 * Sets the modifier Id.
	 * @param modifierId
	 * 					the modifier Id.
	 */
    public void setModifierId(EnumModifier modifierId) {
        this.modifierId = modifierId;
    }

    public boolean isConcreteRelationship() {
        return isConcreteRelationship;
    }

    public void setConcreteRelationship(boolean isConcreteRelationship) {
        this.isConcreteRelationship = isConcreteRelationship;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = result * prime + ((id == null) ? 0 : id.hashCode());
        result = result * prime + ((sourceConcept.getId() == null) ? 0 : sourceConcept.getId().hashCode());
        result = result * prime + ((destinationConcept.getId() == null) ? 0 : destinationConcept.getId().hashCode());
        result = result * prime + ((type.getId() == null) ? 0 : type.getId().hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(null == obj)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Relationship relationship = (Relationship) obj;
        if(id == null && relationship.id != null)
            return false;
        if(!id.equals(relationship.id))
            return false;
        return true;
    }
}
