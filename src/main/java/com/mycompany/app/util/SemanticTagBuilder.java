package com.mycompany.app.util;

public class SemanticTagBuilder {
    
    private static StringBuilder output = new StringBuilder();
    private static char[] arr = null;
    private static int i = 0;
    private static char c;
    private static String semanticTag = null;

    /**
	 * Function to retrieve the Semantic Tag/hierarchy information from the 
	 * FSN of the concept.
	 * @param fsn
	 * 			 FSN or Fully Specified Name.
	 * @return the Semantic Tag/hierarchy {@link String} of the concept.
	 */
    public static String buildSemanticTagFromString(String fsn) {
        if(null == fsn)
            return null;
        else if(output == null)
            output = new StringBuilder();
        else if(output.length() > 0) 
            output.delete(0, output.length());

        arr = fsn.toCharArray();
        i = arr.length - 2;
        c = arr[i];

        while(c != '(' && i != 0) {
            output.append(c);
            c = arr[--i];
        }

        semanticTag = output.reverse().toString().toLowerCase();
        if (semanticTag.contains(")"))
            if(semanticTag.indexOf(")") == semanticTag.length() - 1)
                semanticTag = semanticTag.substring(0, semanticTag.length()-1);

        return semanticTag;
    }   

}
