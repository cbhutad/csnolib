package com.mycompany.app.commons;

public final class Constants {
    
    /*
	 * Class names
	 */
	public static  String DESCRIPTION_CLASS = "Description";
	public static  String CONCEPT_CLASS= "Concept";
	public static  String RELATIONSHIP_CLASS = "Relationship";
	public static  String RELATIONSHIP_TC_CLASS="Relationship_TC";
	
	/*
	 * India maintained module/concept identifier
	 */
	public static final String INDIA_MODULE_ID = "683851000189105";
	
	/*
	 * Table Column names
	 */
	public static final String ID = "id";
	public static final String ACTIVE = "active";
	public static final String CONCEPTID = "conceptId";
	public static final String TYEPID = "typeId";
	public static final String TERM = "term";
	public static final String HIERARCHY = "hierarchy";
	public static final String IS_PREFERRED_TERM = "isPreferredTerm";
	public static final String CONCEPT_STATE = "conceptState";
	public static final String CONCEPT_FSN="conceptFsn";
	public static final String  PREFERRED_NAME = "PreferredName";
	public static final String DEFINITIONSTATUS="definitionStatus";
	public static final String DESCRIPTION_MODID="descriptionModId";
	//Relationship table data  
	public static final String EFFECTIVETIME="effectiveTime";
	public static final String MODULEID="moduleId";
	public static final String RELATIONSHIPGROUP="relationshipGroup";
	public static final String CHARACTERISTICTYPEID="characteristicTypeId";
	public static final String MODIFIERID="modifierId";
	public static final String SOURCE = "source";
	public static final String DESTINATION = "destination";
	public static final String TYPE = "type";	
	
	public static String CONCEPT_CHECKPONT = "0";
	public static String DESC_CHECKPONT = "1";
	
	public static final String SEARCH_TERM = "SearchTerm";
	public static final String STATE = "State";
	public static final String SEMANTIC_TAG = "semantictag"; //no camelCase as analyzer lower cases
	public static final String RELATION_TYPE = "relationType";
	public static final String RELATION_TYPE_ALL = "relationtypeall"; //no camelCase as analyzer lower cases 
	public static final String ACCEPTABILITY = "Acceptability";
	public static final String MAX_RECORDS = "MaxRecords";
	public static final String LANG_REFSET = "langRefset";
	public static final String FULL_CONCEPT = "fullConcept";
	public static final String REFSET_ID = "refsetId";
	public static final String REFSET = "Refset";
	public static final String CREFSET = "cRefset";
	public static final  String REFSETTERM = "RefsetTerm";
	public static final  String DISTINCT = "Distinct";
	public static final  String REFSETCOUNT = "RefsetCount";
	/*
	 * Table Column names of TransClosure table
	 */
	public static final String CHILDID = "childId";
	public static final String PARENTID="parentId";
	public static final String COUNTOFCHILDREN = "countOfChildrens";
	public static final String ISIMMEDIATECHILDREN = "isImmediateChildren";
	
	/*
	 * Table Column names of simple map table
	 */
	public static final String REFERENCEDCOMPONENT="referencedComponent";
	public static final String SIMPLE_MAP_TARGET="simpleMapTarget";
	
	/*
	 * Data fetching constants
	 */
	public static int DESCRIPTION_ALL_RECORDS = 2651408;
	public static int RELATIONSHIP_ALL_RECORDS = 2628082;
	public static int RELATIONSHIP_TC_ALL_RECORDS=5735219;
	public static  int CONCRETE_RELATIONSHIP_ALL_RECORDS = 28986;
	public static  int RELATIONSHIP_EXTENSION_RECORDS_COUNT=0;
	public static  int CONCRETE_RELATIONSHIP_EXTENSION_RECORDS_COUNT=0;
	public static  int SIMPLEMAP_RECORDS_COUNT=0;
	public static  int SEMANTIC_ALL_RECORDS=500;
	public static int RELATION_TYPE_ALL_RECORDS=300;
	public static final int TOTAL_HIT_THRESHOLD=Integer.MAX_VALUE;
	
	/*
	 * Check Index Directory Path
	 */
	public static final String TCINDEX="TCIndex";
	public static final String SNOINDEX="SnoIndex";
	public static final String REFINDEX="RefIndex";
	public static final String SEMINDEX="SemIndex";
	public static final String REL_TYPE_INDEX="RelTypeIndex";
	public static final String PARENTDIR="Directory";
	public static final String CHILDREN="Children";
	public static final String PARENT="Parent";
	public static final String RELATION_INDEX="RelIndex";
	public static final String MAP_INDEX="MapIndex";
	public static final String ATTRIBUTE_TYPE="AttributeType";
	public static final String SIMPLE_MAP_INDEX="SimpleMapIndex";
		
	/*
	 * Index Offset
	 * 
	 */
	public static final int INDEX_OFFSET = 200000;
	
	/*path for H2 DB directory */
	public static final String DATA_DIR = "data.directory";
	
	/*
	 * Error Log Directory
	 */
	public static final String ERROR_LOG_DIR = "log.directory";
	
	/*
	 * SNOMED CT International release directory path
	 * */
	public static final String SNOMED_INT_DIR = "snomed.int.directory";
	
	/*
	 * REFSET release directory path
	 * */
	public static final String REFSET_DIR = "refset.directory";
	
	public static final String SEARCH_RETURNLIMIT ="search.returnlimit";
	public static final String SUGGEST_RETURNLIMIT ="suggest.returnlimit";
	public static final String TOP_SUGGEST_RETURNLIMIT ="top.suggest.returnlimit";
	
	public static final String ERROR_LOG_FILE_NAME = "CSNOErrors.txt";
	public static final String MESSAGE_LOG_FILE_NAME = "CSNOLog.txt";
	
	public static final String NEW_LINE = "\r\n";
	public static final String TAB = "\t";
	public static final String CLASS_NAME = "Class Name: ";
	public static final String METHOD_NAME = "Method Name: ";
	public static final String LINE_NUMBER = "Line Number: ";
	public static final String MESSAGE_DESCRIPTION = "Message: ";
	public static final String[] BRACKETS = {"(",")","[","]"};

}
