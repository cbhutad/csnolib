package com.mycompany.app.factory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;

import com.mycompany.app.commons.Constants;
import com.mycompany.app.util.LoggerUtility;
import com.mycompany.app.util.PropertyReader;

/**
 * Factory implementation of IndexReader
 * Factory method implementation for IndexReader for getting singleton object for IndexReader
 * to read index from given directory.
 */

public class IndexReaderFactory {

	//for holding singleton instance of IndexReader
	private static Map<String, IndexReader> indexReaderMap = new HashMap<>(10);
	private Directory directory = null;
	private IndexReader indexReader = null;
	private static IndexReaderFactory indexReaderFactory = null;

	//private constructor
	private IndexReaderFactory() {}

	/**
	 * Return singleton {@link IndexReaderFactory} object.
	 */
	public static IndexReaderFactory getInstance() {
		if(indexReaderFactory != null)
			return indexReaderFactory;
		else {
			indexReaderFactory = new IndexReaderFactory();
			return indexReaderFactory;
		}
	}

	/**
	 * It will be used in reinitializing indexReaderMap object
	 * @return {@link Map<String, @link IndexReader>} of string and IndexReader object
	 */
	private Map<String, IndexReader> getIndexReaderMap() {
		if(indexReaderMap == null)
			indexReaderMap = new HashMap<>(10);
		return indexReaderMap;
	}

	/**
	 * Returns unique singleton {@link IndexReader} object.
	 * @param refsetId Identifier of Refset/Indexed Directory
	 * @return instance of {@link IndexReader} respective of refsetId/ indexed dir.
	 * @throws IOException if any error occurs while processing
	 */
	public IndexReader getIndexReader(String refsetId) throws IOException {
		PropertyReader.loadSystemProperties();
		if(indexReaderMap == null)
			indexReaderMap = this.getIndexReaderMap();
		if(!indexReaderMap.containsKey(refsetId)) {
			directory = getDirectory(refsetId);
			synchronized(this) {
				if(!indexReaderMap.containsKey(refsetId)) {
					indexReader = DirectoryReader.open(directory);
					indexReaderMap.put(refsetId, indexReader);
				} else {
					indexReader = indexReaderMap.get(refsetId);
				}
			}
		} else {
			indexReader = DirectoryReader.openIfChanged((DirectoryReader) indexReaderMap.get(refsetId));
			if(indexReader != null) {
				indexReaderMap.get(refsetId).close();
				indexReaderMap.put(refsetId, indexReader);
				return indexReader;
			} else {
				indexReader = indexReaderMap.get(refsetId);
			}
		}
		return indexReader;
	}

	/**
	 * Returns {@link Directory} object.
	 * @param refsetId Identifier of Refset/Indexed Directory
	 * @return instance of {@link Directory} respective to refsetId/ indexed dir
	 * @throws IOException if any error occurs while processing.
	 */
	private Directory getDirectory(String refsetId) throws IOException {
		PropertyReader.loadSystemProperties();
		synchronized(this) {
			if(refsetId == null) {
				directory = new MMapDirectory(Paths.get(new File(PropertyReader.getProperty(Constants.DATA_DIR) + File.separator + Constants.SNOINDEX).getAbsolutePath()));
			} else if(Constants.SNOINDEX.equals(refsetId) || Constants.RELATION_INDEX.equals(refsetId) || Constants.TCINDEX.equals(refsetId) || Constants.REFINDEX.equals(refsetId) || Constants.SIMPLE_MAP_INDEX.equals(refsetId) || Constants.SEMINDEX.equals(refsetId) || Constants.REL_TYPE_INDEX.equals(refsetId)) {
				directory = new MMapDirectory(Paths.get(new File(PropertyReader.getProperty(Constants.DATA_DIR) + File.separator + refsetId).getAbsolutePath()));
			} else {
				directory = new MMapDirectory(Paths.get(new File(PropertyReader.getProperty(Constants.DATA_DIR) + File.separator + Constants.REFINDEX).getAbsolutePath()));
			}
			return directory;
		}
	}

	/**
	 * Close all opened {@link IndexReader} created by getIndexReader() in {@link IndexReaderFactory} class
	 */
	private void closeIndexReaders() {
		Logger logger = LogManager.getLogger(IndexReaderFactory.class);
		logger.info(LoggerUtility.Log, "closeIndexReaders() method has been call by GC");
		try {
			if(indexReaderMap != null) {
				for(String refsetId: indexReaderMap.keySet()) {
					if(refsetId != null)
						indexReaderMap.get(refsetId).close();
				}
			}
		} catch(IOException ex) {
			logger.error(LoggerUtility.Exception, LoggerUtility.createErrorMessage(ex));
		} finally {
			indexReaderMap = null;
		}
	}

	@Override
	public void finalize() {
		Logger logger = LogManager.getLogger(IndexReaderFactory.class);
		logger.info(LoggerUtility.Log, "destroying IndexReaderFactory object");
		this.closeIndexReaders();
	}
}
