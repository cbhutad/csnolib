package com.mycompany.app.model;

/**
 * Class inherited from {@link VersionedComponent} class.
 * Provides scope to associate alternate identifiers from other schemes
 * with any SNOMED CT Component. It is currently not in use.
 */
public class Identifier extends VersionedComponent {
    
    /**
	 * Identifier of the concept from the metadata hierarchy that
	 * represents the scheme to which the identifier belongs .
	 */
	private String identifierSchemeId;

    /**
	 * Representation of the alternate identifier in the scheme.
	 */
	private String alternateIdentifier;

    /**
	 * SNOMED CT Concept Id to which the alternate identifier is
	 * associated.
	 */
	private String referencedComponentId;

    /**
	 * Returns the Id of the identifier scheme.
	 * @return identifierSchemeId
	 */
	public String getIdentifierSchemeId() {
		return identifierSchemeId;
	}
	/**
	 * sets the id of the identifier scheme.
	 * @param identifierSchemeId
	 *  						Id of identifier scheme.
	 */
	public void setIdentifierSchemeId(String identifierSchemeId) {
		this.identifierSchemeId = identifierSchemeId;
	}

    /**
	 * Returns id of the alternate identifier in other scheme.
	 * @return alternate Identifier.
	 */
	public String getAlternateIdentifier() {
		return alternateIdentifier;
	}
	/**
	 * Sets the alternate identifier value.
	 * @param alternateIdentifier
	 * 							Id of the alternate identifier in other scheme.
	 */
	public void setAlternateIdentifier(String alternateIdentifier) {
		this.alternateIdentifier = alternateIdentifier;
	}

    /**
	 * Returns the SNOMED CT Component id associated with the alternate
	 * identifier.
	 * @return referencedComponentId
	 * 								SNOMED CT Component id associated with the alternate
	 * 								identifier.
	 */
	public String getReferencedComponentId() {
		return referencedComponentId;
	}
	/**
	 * Sets the SNOMED CT Component id associated with the alternate
	 * identifier .
	 * @param referencedComponentId
	 * 								SNOMED CT Component id associated with the alternate
	 *								identifier.
	 */
	public void setReferencedComponentId(String referencedComponentId) {
		this.referencedComponentId = referencedComponentId;
	}

}
