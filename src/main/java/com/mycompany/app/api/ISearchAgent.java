package com.mycompany.app.api;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import com.mycompany.app.commons.EnumDirection;
import com.mycompany.app.model.CompositeDescription;
import com.mycompany.app.model.Concept;
import com.mycompany.app.model.Refset;
import com.mycompany.app.model.Relationship;
import com.mycompany.app.model.RelationshipTC;
import com.mycompany.app.model.SimpleMapRefsetResult;

/**
 * Search API
 */
public interface ISearchAgent extends Serializable {

	/**
	 * Searches given term/text or concept id in the SNOMED CT repository based on provided component state, semantic tag, acceptability.
	 * @param queryParams Hashmap containing key value pair of search parameters on the basis of which search criteria will be decided. Parameters are as follows,
	 *  <br>SearchTerm - {@link String} term/text or Concept ID to be searched in SNOMED CT repository.<br><br>
	 *  State - {@link EnumState} State of a component.<br>
	 *            If enumState is,<br>
	 *  		  active - It will return only active Components.<br>
	 * 			  inactive - It will return only inactive Components.<br>
	 * 			  both - It will return both active and inactive Components.<br><br>
	 *  SemanticTag -
	 *            {@link EnumSemanticTag} Refers to the Semantic Tag in which SNOMED CT term/text or concept id will be searched. <br>
	 *            If enumSemanticTag is,<br>
	 *            all - It will search SNOMED CT term in all the Semantic Tags.<br>
	 *            For other possible values please refer {@link EnumSemanticTag}.<br><br>
	 *  Acceptability -
	 *            {@link EnumAcceptability} Represents the type of description i.e. whether the search results contains preferred terms or
	 *            preferred terms other than FSN or only acceptable terms and so on.<br>
	 *            If enumAcceptability is, <br>
	 *            all - refers to all description for a concept (both preferred and acceptable).<br>
	 *  		  preferred - refers to all preferred description for a concept (including FSN). <br>
	 *  		  preferredexcludingfsn - refers to SNOMEDCT description for preferred terms excluding FSN.<br>
	 *  		  synonyms - refers to all description for a concept (excluding FSN).<br>
	 *  		  acceptable- refers to all description for a concept excluding preferred.<br><br>
	 *  returnLimit/MaxRecords - Maximum number of matching terms to be fetched.<br><br>
	 *  groupByConcept - boolean value, if true, then method will return set of {@link CompositeDescription} which will contain one
	   			description from one concept.<br><br>
	 *  @return Set of {@link CompositeDescription} matching the given term/text or Concept ID.<br>
	 */
	public Set<CompositeDescription> search(HashMap<String, List<String>> queryParams,boolean fullConcept);

	/**
	 * Searches given conceptId in the SNOMED CT repository<br>
	 * @param conceptId String it will search on the basis of given conceptId and also use for checking
	 * the entered conceptId is valid or not<br>
	 * @return Object of {@link Concept} respected to given conceptId
	 * @throws IOException
	 * @throws ParseException
	 * @throws org.apache.lucene.queryparser.classic.ParseException
	 *
	 */
	public Concept search(final String conceptId) throws ParseException, IOException, org.apache.lucene.queryparser.classic.ParseException;

	/**
	 * Searches given concept id in the SNOMED CT repository based on provided relationship type, direction.
	 * @param
	 *  conceptId - {@link String} Concept ID to be searched in SNOMED CT repository.<br>
	 * 	enumRelationshipType - {@link EnumRelationshipType} Type of relationship.<br>
	 *            If enumRelationshipType is,<br>
	 *  		  is a - It will return only "is a" relation type Components.<br>
	 * 			  all - It will return only "all" relation type Components.<br>
	 * 			  finding site - It will return only "finding site" relation type Components.<br>
	 *  directions- {@link EnumDirection} Relationship direction i.e whether the specified concept Id is the source or destination
	 * 				Possible values for directions is,<br>
	 * 				source - concept Id is a source<br>
	 * 				destination - concept Id is a destination
	 *  @return List of {@link Relationship} matching the given term/text or Concept ID.<br>
	 */
	public List<Relationship> getRelationships(String conceptId, String relationshipType,EnumDirection directions);

	/**
	 * Searches the descendent of concept id.
	 * @param id
	 * concept id in the SNOMED CT
	 * @return Set of {@link Relationship} matching the given Concept ID.
	 * @throws ParseException
	 */
	public Set<Concept> searchTC(String id) throws ParseException;

	/**
	 * Searches the concepts of attribute name and attribute value.
	 * @param queryParams
	 * @return  List of {@link Relationship} matching the given Concept ID.
	 * @throws IOException
	 * @throws ParseException
	 * @throws org.apache.lucene.queryparser.classic.ParseException
	 */
	public List<Relationship> getConcepts(HashMap<String, List<String>> queryParams)
	        throws IOException, ParseException, org.apache.lucene.queryparser.classic.ParseException;


	/**
	 * It will return all semantic tags based on provided SNOMED CT RF2
	 * @return Set<{@link String}> Set of {@link String} containing semantic tags
	 * @throws ParseException
	 * @throws IOException
	 * @throws org.apache.lucene.queryparser.classic.ParseException
	 */
	public Set<String> getSemanticTags() throws IOException, org.apache.lucene.queryparser.classic.ParseException;

	/**
	 * It will return all relationship type based on provided SNOMED CT RF2
	 * @return List<{@link String}> List of {@link String} containing relationship type name
	 * @throws ParseException
	 * @throws IOException
	 * @throws org.apache.lucene.queryparser.classic.ParseException
	 */
	public List<String> getRelationType() throws IOException, org.apache.lucene.queryparser.classic.ParseException;



	/**
	 * Searches the simpleMapTarget of refsetComponentId provided .
	 * @param queryParams
	 * @return  SimpleMapRefsetResult mapped to the given Refset Component ID.
	 * @throws IOException
	 * @throws ParseException
	 * @throws org.apache.lucene.queryparser.classic.ParseException
	 */
	public SimpleMapRefsetResult searchSimpleMap(final String refsetComponentId) throws ParseException, IOException, org.apache.lucene.queryparser.classic.ParseException;

	/**
	 * Searches for the immediate children descriptions.
 	 *@param conceptId
 	 *				Concept Id whose immediate children to search
	 * @return Set of {@link RelationshipTC} matching the given Concept ID.<br>
	 * @throws IOException if any error occurs while processing.
	 * @throws java.text.ParseException
	 */
	public Set<RelationshipTC> getIsARelationshipsWithCount(String conceptId);


	/**
	 * Returns a {@link Set} of {@link Refset}s for given concept identifier.	 *
	 * @return Set of {@link Refset}s.
	 */
	public Set<Refset> getAllRefset() throws org.apache.lucene.queryparser.classic.ParseException, IOException, ParseException;

	/**
	 * Returns a {@link Set} of all Extensions {@link Refset}s.	 *
	 * @return Set of {@link Refset}s.
	 */
	public Set<Refset> getAllExtension() throws org.apache.lucene.queryparser.classic.ParseException, IOException, ParseException;

	/**
	 * Returns a {@link Set} of all Refset Members {@link Concept}s for given concept refset identifier.
	 * @param id SNOMED CT concept identifier.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getRefsetMember(String refsetId)
				throws org.apache.lucene.queryparser.classic.ParseException, IOException, ParseException;

	/**
	 * Returns a {@link Set} of all Extension Members {@link Concept}s for given concept extension identifier.
	 * @param id SNOMED CT concept identifier.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getExtensionMember(String moduleId, int pageNumber)
				throws org.apache.lucene.queryparser.classic.ParseException, IOException, ParseException;

}
