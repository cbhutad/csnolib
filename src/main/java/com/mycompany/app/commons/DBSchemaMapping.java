package com.mycompany.app.commons;

public class DBSchemaMapping {
    
    /*
	 * Tables
	 */
	public static final String CONCEPT = "sct2_concept";
	public static final String RELATIONSHIP = "sct2_relationship";
	public static final String DESCRIPTION = "sct2_description";
	public static final String IDENTIFIER = "sct2_identifier";
	public static final String REFSETLANG = "der2_refset_lang";
	public static final String TRANSCLOSURE = "sct2_relationships_tc";
	public static final String REFSET = "der2_refset";
	public static final String cREFSET = "der2_crefset";
	public static final String TEXTDEFINITION = "sct2_textdefinition";
	public static final String SIMPLEMAP = "der2_refset_simplemap";
	public static final String CONCRETE_RELATIONSHIP = "sct2_concrete_relationship";
	public static String DESCRIPTION_TEXTDEFINITION_CURRENTID = "0";
	public static String RELATIONSHIP_CURRENTID = "0";
	public static String CONCRETE_RELATIONSHIP_CURRENTID = "0";
	public static String TRANSCLOSURE_CURRENTID = "0";
	
	/*
	 * Columns
	 */
    // Concept
	public static final String CONCEPT_ID = CONCEPT+".id";
	public static final String CONCEPT_EFFECTIVE_TIME = CONCEPT+".effectiveTime";
	public static final String CONCEPT_ACTIVE = CONCEPT+".active";
	public static final String CONCEPT_MODULE_ID = CONCEPT+".moduleId";
	public static final String CONCEPT_DEFINITION_STATUS_ID = CONCEPT+".definitionStatusId";
	public static final String CONCEPT_DEFINITION_STATUS = CONCEPT+".definitionStatusId";
	
    //Relationship
	public static final String RELATIONSHIP_ID = RELATIONSHIP+".id";
	public static final String RELATIONSHIP_EFFECTIVE_TIME = RELATIONSHIP+".effectiveTime";
	public static final String RELATIONSHIP_ACTIVE = RELATIONSHIP+".active";
	public static final String RELATIONSHIP_SOURCE_ID = RELATIONSHIP+".sourceId";
	public static final String RELATIONSHIP_DESTINATION_ID = RELATIONSHIP+".destinationId";
	public static final String RELATIONSHIP_RELATIONSHIP_GROUP = RELATIONSHIP+".relationshipGroup";
	public static final String RELATIONSHIP_TYPEID = RELATIONSHIP+".typeId";
	public static final String RELATIONSHIP_CHARACTERISTIC_TYPE_ID = RELATIONSHIP+".characteristicTypeId";
	public static final String RELATIONSHIP_MODIFIER_ID = RELATIONSHIP+".modifierId";
	public static final String RELATIONSHIP_MODULE_ID = RELATIONSHIP+".moduleId";
	
    // Description
	public static final String DESCRIPTION_ID = DESCRIPTION+".id";
	public static final String DESCRIPTION_EFFECTIVE_TIME = DESCRIPTION+".effectiveTime";
	public static final String DESCRIPTION_ACTIVE = DESCRIPTION+".active";
	public static final String DESCRIPTION_MODULE_ID = DESCRIPTION+".moduleId";
	public static final String DESCRIPTION_CONCEPT_ID = DESCRIPTION+".conceptId";
	public static final String DESCRIPTION_LANGUAGE_CODE = DESCRIPTION+".languageCode";
	public static final String DESCRIPTION_TYPE_ID = DESCRIPTION+".typeId";
	public static final String DESCRIPTION_TERM = DESCRIPTION+".term";
	public static final String DESCRIPTION_CASE_SIGNIFICANCE_ID = DESCRIPTION+".caseSignificanceId";
	
    // Text Definition
	public static final String TEXTDEFINITION_ID=TEXTDEFINITION+".id";
	public static final String TEXTDEFINITION_EFFECTIVE_TIME=TEXTDEFINITION+".effectiveTime";
	public static final String TEXTDEFINITION_ACTIVE=TEXTDEFINITION+".active";
	public static final String TEXTDEFINITION_MODULEID=TEXTDEFINITION+".moduleId";
	public static final String TEXTDEFINITION_CONCEPTID=TEXTDEFINITION+".conceptId";
	public static final String TEXTDEFINITION_LANGUAGE_CODE=TEXTDEFINITION+".languageCode";
	public static final String TEXTDEFINITION_TYPE_ID=TEXTDEFINITION+".typeId";
	public static final String TEXTDEFINITION_TERM=TEXTDEFINITION+".term";
	public static final String TEXTDEFINITION_CASE_SIGNIFICANCE_ID=TEXTDEFINITION+".caseSignificanceId";
	
	// Identifier
	public static final String IDENTIFIER_IDENTIFIER_SCHEME_ID = IDENTIFIER+".identifierSchemeId";
	public static final String IDENTIFIER_ALTERNATE_IDENTIFIER = IDENTIFIER+".alternateIdentifier";
	public static final String IDENTIFIER_EFFECTIVE_TIME = IDENTIFIER+".effectiveTime";
	public static final String IDENTIFIER_ACTIVE = IDENTIFIER+".active";
	public static final String IDENTIFIER_MODULE_ID = IDENTIFIER+".moduleId";
	public static final String IDENTIFIER_REFERENCED_COMPONENT_ID = IDENTIFIER+".referencedComponentId";

	// REFSET LANG
	public static final String REFSETLANG_ID = REFSETLANG +".id";
	public static final String REFSETLANG_EFFECTIVE_TIME= REFSETLANG +".effectiveTime";
	public static final String REFSETLANG_ACTIVE= REFSETLANG +".active";
	public static final String REFSETLANG_MODULE_ID= REFSETLANG +".moduleId";
	public static final String REFSETLANG_REFSET_ID= REFSETLANG +".refsetId";
	public static final String REFSETLANG_REFERENCED_COMPONENT_ID= REFSETLANG +".referencedComponentId";
	public static final String REFSETLANG_ACCEPTABILITY_ID= REFSETLANG +".acceptabilityId";
	
	
	// TRANSITIVE CLOSURE TABLE FOR RELATIONSHIPS
	public static final String TRANSCLOSURE_PARENT_ID = TRANSCLOSURE + ".parentId";
	public static final String TRANSCLOSURE_CHILD_ID = TRANSCLOSURE+ ".childId";
	
	
	// REFSET 
	public static final String REFSET_ID = REFSET+".id";
	public static final String REFSET_EFFECTIVE_TIME = REFSET+".effectiveTime";
	public static final String REFSET_ACTIVE = REFSET+".active";
	public static final String REFSET_MODULE_ID = REFSET+".moduleId";
	public static final String REFSET_REFSET_ID = REFSET+".refsetId";
	public static final String REFSET_REFERENCED_COMPONENT_ID = REFSET+".referencedComponentId";
	
	//SIMPLEMAP
	public static final String SIMPLEMAP_ID = SIMPLEMAP+".id";
	public static final String SIMPLEMAP_EFFECTIVE_TIME = SIMPLEMAP+".effectiveTime";
	public static final String SIMPLEMAP_ACTIVE = SIMPLEMAP+".active";
	public static final String SIMPLEMAP_MODULE_ID = SIMPLEMAP+".moduleId";
	public static final String SIMPLEMAP_REFSET_ID = SIMPLEMAP+".refsetId";
	public static final String SIMPLEMAP_REFERENCED_COMPONENT_ID = SIMPLEMAP+".referencedComponentId";
	public static final String SIMPLEMAP_MAP_TARGET = SIMPLEMAP+".mapTarget";
	
	// cREFSET 
    public static final String cREFSET_ID = cREFSET+".id";
    public static final String cREFSET_EFFECTIVE_TIME = cREFSET+".effectiveTime";
    public static final String cREFSET_ACTIVE = cREFSET+".active";
    public static final String cREFSET_MODULE_ID = cREFSET+".moduleId";
    public static final String cREFSET_REFSET_ID = cREFSET+".refsetId";
    public static final String cREFSET_REFERENCED_COMPONENT_ID = cREFSET+".referencedComponentId";
    public static final String cREFSET_TARGETID = cREFSET+".targetId";
		
	// CONCRETE RELATIONSHIP
    public static final String CONCRETE_RELATIONSHIP_ID = CONCRETE_RELATIONSHIP+".id";
    public static final String CONCRETE_RELATIONSHIP_EFFECTIVE_TIME = CONCRETE_RELATIONSHIP+".effectiveTime";
    public static final String CONCRETE_RELATIONSHIP_ACTIVE = CONCRETE_RELATIONSHIP+".active";
    public static final String CONCRETE_RELATIONSHIP_SOURCE_ID = CONCRETE_RELATIONSHIP+".sourceId";
    public static final String CONCRETE_RELATIONSHIP_DESTINATIONVALUE = CONCRETE_RELATIONSHIP+".destinationValue";
    public static final String CONCRETE_RELATIONSHIP_RELATIONSHIP_GROUP = CONCRETE_RELATIONSHIP+".relationshipGroup";
    public static final String CONCRETE_RELATIONSHIP_TYPEID = CONCRETE_RELATIONSHIP+".typeId";
    public static final String CONCRETE_RELATIONSHIP_CHARACTERISTIC_TYPE_ID = CONCRETE_RELATIONSHIP+".characteristicTypeId";
    public static final String CONCRETE_RELATIONSHIP_MODIFIER_ID = CONCRETE_RELATIONSHIP+".modifierId";
    public static final String CONCRETE_RELATIONSHIP_MODULE_ID = CONCRETE_RELATIONSHIP+".moduleId";

}
