package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumCharacteristicType {
    
    INFERRED_RELATIONSHIP("900000000000011006"),
	ADDITIONAL_RELATIONSHIP("900000000000227009"),
	STATED_RELATIONSHIP("900000000000010007"),
	QUALIFYING_RELATIONSHIP("900000000000225001"),
	DEFINING_RELATIONSHIP("900000000000006009");
	
	private static final Map<String, EnumCharacteristicType> lookup = new ConcurrentHashMap<String, EnumCharacteristicType>();
    
    static {
        for (EnumCharacteristicType d : EnumCharacteristicType.values())
            lookup.put(d.getValue(), d);
    }
    	
	private String value;
	
    private EnumCharacteristicType( String value) {
		this.setValue(value);
	}
	
	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumCharacteristicType} enumeration for given string value
	 */
	public static EnumCharacteristicType getEnum(String val) {
		return lookup.get(val);
	}

}
