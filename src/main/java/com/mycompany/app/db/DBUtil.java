package com.mycompany.app.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mycompany.app.commons.DBSchemaMapping;
import com.mycompany.app.commons.EnumAcceptabilityId;
import com.mycompany.app.commons.EnumDescType;
import com.mycompany.app.commons.EnumRefset;
import com.mycompany.app.util.LoggerUtility;

/**
 * Database resource provider utility for query generation and data fetching.
 */
public class DBUtil {

    private static final Logger logger = LogManager.getLogger(DBUtil.class);

    /**
	 * Gets instance of {@link Statement} for database operation.
	 * @param connection
	 * 				 	Connection object.
	 * @return instance of {@link Statement}.
	 * @throws SQLException
	 */
    public static Statement getStatement(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        return statement;
    }

    /**
	 * Closes given instance of {@link ResultSet}.
	 * @param resultSet
	 * 					instance of {@link ResultSet}.
	 * @throws SQLException
	 */
    public static void closeResultSet(ResultSet resultSet) throws SQLException {
        if(null != resultSet) {
            resultSet.close();
        }
    }

    /**
	 * Closes given instance of {@link Statement}.
	 * @param statement
	 * 					{@link Statement} object.
	 * @throws SQLException
	 */
    public static void closeStatement(Statement statement) throws SQLException {
        if(null != statement) {
            statement.close();
        }
    }

    /**
	 * Closes given instance of {@link Connection}.
	 * @param connection
	 * 					{@link Connection} class object.
	 * @throws SQLException
	 */

    public static void closeConnection(Connection connection) throws SQLException {
        if(null != connection) {
            connection.close();
        }
    }

    /**
	 * Fetches all rows from specified database table associated with given instance of {@link Statement}.
	 * @param tableName
	 * 					 table in which the query has to be fired.
	 * @param statement
	 * 					 {@link Statement} instance.
	 * @return result set obtained on firing the query on DB.
	 * @throws SQLException
	 */
    public static ResultSet retrieveAll(String tableName, Statement statement) throws SQLException {
        return statement.executeQuery("Select * from " + tableName + ";");
    }

    /**
	 * Fetches specified columns data using offset and returnLimit values.
	 * @param tableName
	 * 					 table in which the query has to be fired.
	 * @param columnArray
	 * 					 List of columns.
	 * @param statement
	 * 					 {@link Statement} instance.
	 * @param offset
	 * 					  the offset.
	 * @param returnLimit
	 * 					  the maximum number of results to be obtained.
	 * @return result set obtained on the execution of the query.
	 * @throws SQLException
	 */
    public static ResultSet retrieveColumns(String tableName, String[] columnArray, Statement statement, long offset, long returnLimit, boolean isExtensionSelected)  throws SQLException {
        StringBuilder query = new StringBuilder("Select ");
        for(String column : columnArray) {
            query.append(" " + column + ",");
        }
        query.replace(0, query.length(), query.substring(0,query.length() - 1));
        if(isExtensionSelected) {
            query.append(" from " + tableName + " Limit " + offset + ", " + returnLimit + ";");
        } else {
            query.append(" from " + tableName + " Limit " + returnLimit + ";");
        }
        return statement.executeQuery(query.toString());
    }

    /**
	 * Gets count of total number of rows in specified table.
	 * @param tableName
	 * 					Name of the table to be retrieved.
	 * @param statement
	 * 					{@link Statement} instance.
	 * @param moduleId
	 * 					ModuleId concept for extension and refset, pass this if require constrain search. Default value is null
	 * @return totalRows
	 * 					the total number of rows obtained on executing the query.
	 * @throws SQLException
	 */
    public static long getTotalRows(String tableName, Statement statement, String moduleId) throws SQLException {
        ResultSet resultSet = null;
        long numberOfRows = 0;
        
        try {
            if(moduleId != null && moduleId.isEmpty())
                moduleId = null;
            if(tableName != null && DBSchemaMapping.DESCRIPTION.equals(tableName)) {
                if(moduleId != null)
                    tableName = DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.DESCRIPTION_MODULE_ID + "=" + moduleId;
                else
                    tableName = DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID;
                resultSet = statement.executeQuery("Select count(*) from " + tableName + ";");
            } else if(tableName != null && DBSchemaMapping.TEXTDEFINITION.equals(tableName)) {
                tableName = DBSchemaMapping.TEXTDEFINITION + "," + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.TEXTDEFINITION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID;
                resultSet = statement.executeQuery("Select count(*) from " + tableName + ";");
            } else if(tableName != null && DBSchemaMapping.TRANSCLOSURE.equals(tableName)) {
                resultSet = statement.executeQuery("Select count(*) from "+ DBSchemaMapping.TRANSCLOSURE+ " tc where exists (select tc.parentId, tc.childId from " + DBSchemaMapping.CONCEPT + " co where tc.childid=co.id and co.moduleid=" + moduleId +");"); 
            } else {
                resultSet = statement.executeQuery("Select count(*) from " + tableName + ";");
            }
            if(resultSet.next())
                numberOfRows = resultSet.getLong(1);
        } finally {
            DBUtil.closeResultSet(resultSet);
        }
        return numberOfRows;
    }

    /**
	 * Returns fully specified Name (FSN) of a given Concept Id.
     * @param moduleId
     *                  Module Identifier.
     * @return conceptFSN
     *                      Fully specified name for given conceptId.
     * @throws SQLException
	 */
    public static String getConceptFSN(String moduleId, Statement statment) throws SQLException {
        String conceptFSN = null;
        ResultSet resultSet = null;
        String query = null;
        try {
            query = "Select Term from " + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + moduleId + ";";
            resultSet = statment.executeQuery(query);
            if(resultSet.next()) {
                conceptFSN = resultSet.getString(1);
            }
        } catch (SQLException ex) {
            logger.info(LoggerUtility.CONSOLE, "SQLException: {}", ex.getMessage());
            logger.info(LoggerUtility.CONSOLE, "SQLState: {}", ex.getSQLState());
            logger.info(LoggerUtility.CONSOLE, "VendorError: {}", ex.getErrorCode());
            logger.error(LoggerUtility.Exception, LoggerUtility.createErrorMessage(ex));
        } finally {
            DBUtil.closeResultSet(resultSet);
        }
        return conceptFSN;
    }

    /**
	 * Retrieves Concept state of Specified Concept ID.
	 * @param conceptId
	 * 					 concept id.
	 * @param statement
	 * 					 {@link Statement} class instance.
	 * @return {@link ResultSet} that is actually the Concept state for that particular concept id.
	 * @throws SQLException
	 */
    public static ResultSet retrieveConceptState(String conceptId, Statement statement) throws SQLException {
        return statement.executeQuery("Select " + DBSchemaMapping.CONCEPT_ACTIVE + " from " + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.CONCEPT_ID + "=" + conceptId + ";");
    }

    /**
	 * RetrievesConcept details.
	 * @param conceptId
	 * 					 concept id.
	 * @param statement
	 * 					 {@link Statement} class instance.
	 * @throws SQLException
	 */
    public static ResultSet retrieveConceptDetails(String conceptId, Statement statement) throws SQLException {
        return statement.executeQuery("Select " + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.CONCEPT_ACTIVE + "," + DBSchemaMapping.CONCEPT_DEFINITION_STATUS + "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.CONCEPT_ID + " and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + conceptId + " and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1;");
    }

    /**
	 * RetrievesConcept details.
	 * @param conceptId
	 * 					 concept id.
	 * @param languageCode
	 * 					 Description Language Code
	 * @param statement
	 * 					 {@link Statement} class instance.
	 * @throws SQLException
	 */
    public static ResultSet retrieveConceptDetails(String conceptId, String languageCode, Statement statement) throws SQLException {
        return statement.executeQuery("Select " + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.CONCEPT_ACTIVE + "," + DBSchemaMapping.CONCEPT_DEFINITION_STATUS + "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.CONCEPT_ID + " and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + conceptId + " and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_LANGUAGE_CODE + "= '" + languageCode + "' and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1;");
    }

    /**
	 * Retrieves Term and Type ID of description id which is FSN.
	 * @param conceptId
	 * 					 concept identifier.
     * @param languageCode
	 * 					 Description Language Code.
	 * @param statement
	 * 					 {@link Statement} class instance.
	 * @return {@link ResultSet} obtained on DB query execution.
	 * @throws SQLException
	 */
    public static ResultSet retrieveFSNForHeirarchy(String conceptId, String languageCode, Statement statement) throws SQLException {
        return statement.executeQuery("Select " + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.DESCRIPTION_TYPE_ID + " from " + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + conceptId + " and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_LANGUAGE_CODE + "= '" + languageCode + "' and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1;");
    }

    /**
	 * Retrieves Acceptability id of description which indicates preferred term for that description.
	 * @param descriptionId
	 * 						 description id.
	 * @return acceptability id
	 * 						 the refset acceptability id corresponding to that description.
	 * @throws SQLException
	 */
    public static ResultSet retrieveAcceptabilityId(String descriptionId, Statement statement) throws SQLException {
        return statement.executeQuery("Select " + DBSchemaMapping.REFSETLANG_ACCEPTABILITY_ID + " from " + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + "=" + descriptionId + " and " + DBSchemaMapping.REFSETLANG_ACCEPTABILITY_ID + "=" + EnumAcceptabilityId.PREFERRED.getValue() + ";");
    }

    /**
	 * Fetches Description data for the specified Refset Id using offset and returnLimit values.
	 * @param refsetId
	 * 					 identifier of Refset for which data is to be retrieved.

	 * @param statement
	 * 					 {@link Statement} instance.
	 * @param offset
	 * 					  the offset.
	 * @param returnLimit
	 * 					  the maximum number of results to be obtained.
	 * @return result set obtained on the execution of the query.
	 * @throws SQLException
	 */
    public static ResultSet retrieveRefsetColumns(String refsetId, long offset, long returnlimit, Statement statment) throws SQLException {
        return statment.executeQuery("select "+ DBSchemaMapping.DESCRIPTION_ID + "," + DBSchemaMapping.DESCRIPTION_ACTIVE + "," + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "," + DBSchemaMapping.DESCRIPTION_TYPE_ID + "," + DBSchemaMapping.DESCRIPTION_TERM+ "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + "," + DBSchemaMapping.DESCRIPTION_EFFECTIVE_TIME + "," + DBSchemaMapping.DESCRIPTION_CASE_SIGNIFICANCE_ID + "," + DBSchemaMapping.DESCRIPTION_LANGUAGE_CODE+ "," + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + "," + DBSchemaMapping.REFSETLANG_REFSET_ID + "," + DBSchemaMapping.REFSETLANG_ACTIVE + "," + DBSchemaMapping.REFSETLANG_ACCEPTABILITY_ID + "," + DBSchemaMapping.REFSET_REFSET_ID + "," + DBSchemaMapping.REFSET_MODULE_ID + "," + DBSchemaMapping.REFSET_ACTIVE + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.REFSET + "," + DBSchemaMapping.REFSETLANG + "  where " + DBSchemaMapping.REFSET_REFSET_ID + "='" + refsetId + "' and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.REFSET_REFERENCED_COMPONENT_ID +
        " and " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1 and " + DBSchemaMapping.REFSET_ACTIVE + "=1" +";");
    }

    /**
	 * Fetches Description data for the specified Refset Id using offset and returnLimit values.
	 * @param refsetId
	 * 					 identifier of Refset for which data is to be retrieved.

	 * @param statement
	 * 					 {@link Statement} instance.
	 * @param offset
	 * 					  the offset.
	 * @param returnLimit
	 * 					  the maximum number of results to be obtained.
	 * @param moduleIdSet
	 * @return result set obtained on the execution of the query.
	 * @throws SQLException
	 */
    static StringBuilder br  = new StringBuilder();
    public static ResultSet retrieveRefsetColumns(String refsetId, long offset, long returnLimit, Set<String> moduleIdSet, boolean flag, Statement statement) throws SQLException {

        String query = "";

        if(flag) {
            EnumRefset[] r = EnumRefset.values();
            for(int i = 0;i < r.length;i++) {
                if(r[i].getValue().equals("association type") || r[i].getValue().equals("attribute type"))
                    query = query + "Select " + DBSchemaMapping.DESCRIPTION_ID + "," + DBSchemaMapping.DESCRIPTION_ACTIVE + "," + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "," + DBSchemaMapping.DESCRIPTION_TYPE_ID + "," + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + "," + DBSchemaMapping.DESCRIPTION_EFFECTIVE_TIME + "," + DBSchemaMapping.DESCRIPTION_CASE_SIGNIFICANCE_ID + "," + DBSchemaMapping.DESCRIPTION_LANGUAGE_CODE + "," + DBSchemaMapping.cREFSET_REFSET_ID + "," + DBSchemaMapping.cREFSET_MODULE_ID+ "," + DBSchemaMapping.cREFSET_TARGETID + "," + DBSchemaMapping.cREFSET_ACTIVE + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.cREFSET + "  where " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.cREFSET_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.cREFSET_REFSET_ID + "= '" +r[i].getId() + "'";
                if(i != r.length-1)
                    query = query + "union";
                br.append(" and " + DBSchemaMapping.cREFSET_REFSET_ID + "!= '" + r[i].getId() + "'");
            }
            query = query + ";";
        }
        for(String str: moduleIdSet) {
            if(refsetId.equalsIgnoreCase("cRefset")) {
                if(!flag) {
                    query = "Select " + DBSchemaMapping.DESCRIPTION_ID + "," + DBSchemaMapping.DESCRIPTION_ACTIVE + "," + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "," + DBSchemaMapping.DESCRIPTION_TYPE_ID + "," + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + "," + DBSchemaMapping.DESCRIPTION_EFFECTIVE_TIME + "," + DBSchemaMapping.DESCRIPTION_CASE_SIGNIFICANCE_ID + "," + DBSchemaMapping.DESCRIPTION_LANGUAGE_CODE + "," + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + "," + DBSchemaMapping.REFSETLANG_REFSET_ID + "," + DBSchemaMapping.REFSETLANG_ACTIVE + "," + DBSchemaMapping.REFSETLANG_ACCEPTABILITY_ID + "," + DBSchemaMapping.cREFSET_REFSET_ID + "," + DBSchemaMapping.cREFSET_MODULE_ID+ "," + DBSchemaMapping.cREFSET_TARGETID + "," + DBSchemaMapping.cREFSET_ACTIVE + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.cREFSET + "," + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.cREFSET_MODULE_ID + "= '" + str + "' and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.cREFSET_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + br.toString() + " Limit " + offset + "," + returnLimit + ";";
                }
            }
            if(refsetId.equalsIgnoreCase("Refset")) {
                query="Select " + DBSchemaMapping.DESCRIPTION_ID + "," + DBSchemaMapping.DESCRIPTION_ACTIVE + "," + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "," + DBSchemaMapping.DESCRIPTION_TYPE_ID + "," + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + "," + DBSchemaMapping.DESCRIPTION_EFFECTIVE_TIME + "," + DBSchemaMapping.DESCRIPTION_CASE_SIGNIFICANCE_ID + "," + DBSchemaMapping.DESCRIPTION_LANGUAGE_CODE + "," + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + "," + DBSchemaMapping.REFSETLANG_REFSET_ID + "," + DBSchemaMapping.REFSETLANG_ACTIVE + "," + DBSchemaMapping.REFSETLANG_ACCEPTABILITY_ID + "," + DBSchemaMapping.cREFSET_REFSET_ID + "," + DBSchemaMapping.cREFSET_MODULE_ID + "," + DBSchemaMapping.cREFSET_TARGETID + "," + DBSchemaMapping.cREFSET_ACTIVE + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.cREFSET + "," + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.cREFSET_MODULE_ID + "= '" + str + "' and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.cREFSET_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + br.toString() + " Limit " + offset + "," + returnLimit + ";";
            }
        }
        return statement.executeQuery(query);
    }

    /**
     * Returns count of total number of rows of Description Table which are required for a particular Refset.
     * @param moduleId
     * 					module Identifier .
     * @param statement
     * 					{@link Statement} instance.
     * @return refsetId_list
     * 					distinct refsetId obtained on executing the query.
     * @throws SQLException
    */
    @SuppressWarnings("resource")
    public static HashMap<String, String> getDistinctRefsetId(String moduleId,String refsetId,Statement statement) throws SQLException {
        ResultSet resultSet = null;
        HashMap<String,String>refsetId_list = new HashMap<>();
        String query=null;
        try{
            if(refsetId==null) {
                query="Select distinct (" + DBSchemaMapping.REFSET_REFSET_ID + ")," + DBSchemaMapping.DESCRIPTION_TERM + " from " + DBSchemaMapping.REFSET + "," + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.REFSET_MODULE_ID + "="+ moduleId + " and " + DBSchemaMapping.REFSET_REFSET_ID + "=" + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + " and "+ DBSchemaMapping.DESCRIPTION_ACTIVE + "=1 and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + ";";
            } else {
                query="Select " + DBSchemaMapping.REFSET_REFSET_ID + "," + DBSchemaMapping.DESCRIPTION_TERM + " from " + DBSchemaMapping.REFSET + "," + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.REFSET_REFSET_ID + "="+ refsetId + " and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.REFSET_REFSET_ID + " and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1 ;";
            }
            resultSet=statement.executeQuery(query);
            if(!resultSet.next()) {
                query="Select " + DBSchemaMapping.REFSET_REFSET_ID + " from " + DBSchemaMapping.REFSET + " where " + DBSchemaMapping.REFSET_REFSET_ID + "=" + refsetId + " limit 1;";
                resultSet=statement.executeQuery(query);
                while(resultSet.next()){
                    refsetId_list.put(resultSet.getString(1),"Beta version of the reference set");
                }
            }
            else{
                do {
                    refsetId_list.put(resultSet.getString(1),resultSet.getString(2));
                }
                while(resultSet.next());
            }
        }finally{
            DBUtil.closeResultSet(resultSet);
        }
        return refsetId_list;
    }

    /**
     * Returns count of total number of rows of Description Table which are required for a particular Refset .
     * @param moduleId
     * 					module Identifier .
     * @param statement
     * 					{@link Statement} instance.
     * @return refsetId_list
     * 					distinct refsetId obtained on executing the query.
     * @throws SQLException
     */
    public static HashMap<String, String> getDistinctcRefsetId(String moduleId,Statement statement) throws SQLException{
        ResultSet resultSet = null;
        HashMap<String,String> refsetId_list = new HashMap<>();
        try{
            String query="Select distinct (" + DBSchemaMapping.cREFSET_REFSET_ID + ")," + DBSchemaMapping.DESCRIPTION_TERM + " from " + DBSchemaMapping.cREFSET + "," + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.cREFSET_MODULE_ID + "="+ moduleId + " and " + DBSchemaMapping.cREFSET_REFSET_ID + "=" + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + " and "+ DBSchemaMapping.DESCRIPTION_ACTIVE + "=1 and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue()+";";
            resultSet=statement.executeQuery(query);
            
            while(resultSet.next()){
                refsetId_list.put(resultSet.getString(1),resultSet.getString(2));
            }
        }finally{
            DBUtil.closeResultSet(resultSet);
        }
        return refsetId_list;
    }

    /**
	 * Returns count of total number of rows of Description Table which are required for a particular Refset .
	 * @param refsetId
	 * 					Refset Identifier .
	 * @param statement
	 * 					{@link Statement} instance.
	 * @return totalRows
	 * 					the total number of rows obtained on executing the query.
	 * @throws SQLException
	 */
    public static long getTotalRefsetRows(String refsetId, Statement statement) throws SQLException {
        long numberOfRows = 0;
        ResultSet resultSet = null;
        String query = null;
        try {
            if(refsetId.equalsIgnoreCase((DBSchemaMapping.REFSET))) {
                query = "Select count(*) from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.REFSET + "," + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.REFSET_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + ";";
            } else if(refsetId.equalsIgnoreCase(DBSchemaMapping.cREFSET)) {
                query = "Select count(*) from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.cREFSET + "," + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.cREFSET_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1 and " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + ";";
            } else {
                query = "Select count(*) from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.REFSET + " where " + DBSchemaMapping.REFSET_REFSET_ID + "= '" + refsetId + "' and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.REFSET_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1 and " + DBSchemaMapping.REFSET_ACTIVE + "=1" + ";";
            }
            resultSet = statement.executeQuery(query);
            if(resultSet.next())
                numberOfRows = resultSet.getLong(1);
        } finally {
            DBUtil.closeResultSet(resultSet);
        }
        return numberOfRows;
    }

    /**
	 * Returns count of total number of rows of Refset and cRefset Table.
	 * @param refsetId
	 * 					Refset Identifier .
	 * @param statement
	 * 					{@link Statement} instance.
	 * @return totalRows
	 * 					the total number of rows obtained on executing the query.
	 * @throws SQLException
	 */
    @SuppressWarnings("resource")
    public static long getTotalRefsetCount(String refsetId, Statement statement) throws SQLException {
        long numberOfRows = 0;
        ResultSet resultSet = null;
        String query = null;
        try {
            query = "Select count(*) from " + DBSchemaMapping.REFSET + " where " + DBSchemaMapping.cREFSET_REFSET_ID + "=" + refsetId + " and " + DBSchemaMapping.cREFSET_ACTIVE + "=1;";
            resultSet = statement.executeQuery(query);
            if(resultSet.next())
                numberOfRows = resultSet.getLong(1);
            if(numberOfRows == 0) {
                query = "Select count(*) from " + DBSchemaMapping.REFSET + " where " + DBSchemaMapping.REFSETLANG_ACTIVE + "=1 and " + DBSchemaMapping.REFSET_REFSET_ID + "=" + refsetId + ";";
                resultSet = statement.executeQuery(query);
                if(resultSet.next())
                    numberOfRows = resultSet.getLong(1);
            }
        } finally {
            DBUtil.closeResultSet(resultSet);
        }
        return numberOfRows;
    }

    /**
	 * Returns count of total number of rows of an Extension in Concept Table.
	 * @param moduleId
	 * 					Module Identifier .
	 * @return totalRows
	 * 					the total number of rows obtained on executing the query.
	 * @throws SQLException
	 */
    public static long getTotalExtensionCount(String moduleId, Statement statement) throws SQLException {
        long numberOfRows = 0;
        ResultSet resultSet = null;
        String query = null;
        try {
            query = "Select count(*) from " + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.CONCEPT_MODULE_ID + "=" + moduleId + ";";
            resultSet = statement.executeQuery(query);
            if(resultSet.next())
                numberOfRows = resultSet.getLong(1);
        } finally {
            DBUtil.closeResultSet(resultSet);
        }
        return numberOfRows;
    }
}
