package com.mycompany.app.model;

import java.util.Date;
import java.util.List;

public class ConceptDetails {
    
    /**
	 * List of descriptions associated with a particular Concept.
	 */
	private List<CompositeDescription> descriptions; 
	/**
	 * List of relationships associated with a particular Concept.
	 */
	private List<Relationship> relationships; 
	/**
	 * Fully Specified Name of the concept.
	 */
	private String fsn;	
	/**
	 * Identifier of the Concept.
	 */
	private String conceptId;	
	/**
	 * Definition Status of the Concept.
	 */
	private String definitionStatus;	
	/**
	 * Active status of the concept.
	 */
	private boolean active;	
	/**
	 * Effective time corresponding to the concept.
	 */
	private Date effectiveTime;	
	/**
	 * Module Id of the Concept.
	 */
	private String module;
	/**
	 * Parameterized constructor.
	 * @param conceptid
	 * 					 SNOMED CT Concept identifier.
	 * @param fsn
	 * 					 Fully Specified Name(FSN) of the Concept.
	 */
	public ConceptDetails(String conceptid, String fsn)
	{
		this.conceptId = conceptid;
		this.fsn = fsn;
		
	}
	/**
	 * Returns the descriptions (<code>CompositeDescription</code>) corresponding to the Concept.
	 * @return list of {@link CompositeDescription}.
	 */
	public List<CompositeDescription> getDescriptions() {
		return descriptions;
	}
	/**
	 * Sets the descriptions (<code>CompositeDescription</code>) corresponding to the Concept.
	 * @param descriptions
	 * 					 list of {@link CompositeDescription}.
	 */
	public void setDescriptions(List<CompositeDescription> descriptions) {
		this.descriptions = descriptions;
	}
	/**
	 * Returns the relationships (<code>Relationship</code>) corresponding to the Concept.
	 * @return list of {@link Relationship}.
	 */
	public List<Relationship> getRelationships() {
		return relationships;
	}
	/**
	 * Sets the relationships(<code>Relationship</code>) corresponding to the Concept.
	 * @param relationships
	 * 						list of {@link Relationship}.
	 */
	public void setRelationships(List<Relationship> relationships) {
		this.relationships = relationships;
	}
	/**
	 * Returns the Fully Specified Name(FSN) for the Concept.
	 * @return Fully Specified Name(FSN) for the Concept.
	 */
	public String getFsn() {
		return fsn;
	}
	/**
	 * Sets the Fully Specified Name(FSN) for the Concept.
	 * @param fsn
	 * 			 Fully Specified Name(FSN) for the Concept.
	 */
	public void setFsn(String fsn) {
		this.fsn = fsn;
	}
	/**
	 * Returns the SNOMED CT Concept identifier.
	 * @return SNOMED CT Concept identifier.
	 */
	public String getConceptId() {
		return conceptId;
	}
	/**
	 * Sets the SNOMED CT Concept identifier.
	 * @param conceptId
	 * 					 SNOMED CT Concept identifier.
	 */
	public void setConceptId(String conceptId) {
		this.conceptId = conceptId;
	}
	/**
	 * Returns the definition status of the Concept.
	 * @return definitionStatus of the Concept.
	 */
	public String getDefinitionStatus() {
		return definitionStatus;
	}
	/**
	 * Sets the definitionStatus.
	 * @param definitionStatus
	 * 							{@link String} value that refers to the definition 
	 * 							status of the Concept.
	 */
	public void setDefinitionStatus(String definitionStatus) {
		this.definitionStatus = definitionStatus;
	}
	/**
	 * Returns the active status of the Concept.
	 * @return boolean value which is true if status is active and false otherwise.
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * Sets the active status of the concept.
	 * @param active
	 * 				 boolean value which is true if status is active and false otherwise.
	 */
	public void setActive(boolean active) {
		this.active = active;
	}
	/**
	 * Returns the effective time.
	 * @return the effective time (Date) for the concept.
	 */
	public Date getEffectiveTime() {
		return effectiveTime;
	}
	/**
	 * Sets the effective time.
	 * @param effectiveTime
	 * 						 effective time (Date) of the concept.
	 */
	public void setEffectiveTime(Date effectiveTime) {
		this.effectiveTime = effectiveTime;
	}
	/**
	 * Returns the module Id.
	 * @return module Id of the Concept.
	 */
	public String getModule() {
		return module;
	}
	/**
	 * Sets the module Id.
	 * @param module
	 * 				module Id of the concept.
	 */
	public void setModule(String module) {
		this.module = module;
	}
	/**
	 * Parameterized constructor
	 * @param descriptions
	 * 					 list of {@link CompositeDescription}s related to the Concept.
	 * @param relationships
	 * 					 list of {@link Relationship}s related to the Concept.
	 * @param fsn
	 * 					 Fully Specified Name(FSN) of the Concept.
	 * @param conceptId
	 * 					 SNOMED CT Concept identifier.
	 * @param definitionStatus
	 * 					 definition status of the Concept.
	 * @param active
	 * 					 active/inactive status of the Concept.
	 * @param effectiveTime
	 * 					 specifies the inclusive date of the Concept.
	 * @param module	
	 * 					 module Id of the Concept.
	 */
	public ConceptDetails(List<CompositeDescription> descriptions,
			List<Relationship> relationships, String fsn, String conceptId,
			String definitionStatus, boolean active,
			Date effectiveTime, String module) {
		super();
		this.descriptions = descriptions;
		this.relationships = relationships;
		this.fsn = fsn;
		this.conceptId = conceptId;
		this.definitionStatus = definitionStatus;
		this.active = active;
		this.effectiveTime = effectiveTime;
		this.module = module;
	}	

}
