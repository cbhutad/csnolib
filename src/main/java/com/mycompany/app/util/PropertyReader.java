package com.mycompany.app.util;

import java.util.Properties;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


public class PropertyReader {
    
    private static Properties props;

    /**
	 * Loads System properties .
	 * @return Object of {@link Properties}.
	 * @throws IOException - If any exception occurred during IO operation.
	 */
    public static synchronized Properties loadSystemProperties() {
        if(null == props)
            props = new Properties();
        return props;
    }

    /**
	 * Gets property.
	 * @param key
	 * 			 String value representing Key of property.
	 * @return String value of property.
	 */
    public static String getProperty(String key) {
        Logger logger = LogManager.getLogger(PropertyReader.class);
        if(null != key) {
            if(props == null) 
                logger.info("prop is null");
            else
                return props.getProperty(key);
        }
        return null;
    }

    /**
	 * Sets property.
	 * @param key
	 * 			 String value representing Key of property.
	 */
    public static void setProperty(String key, String value) {
        if(null != key) {
            props.setProperty(key, value);
        }
    }
}
