package com.mycompany.app.model;

import java.util.Date;

/**
 * Component is an abstract class which has been derived from {@link VersionedComponent} class.
 * Component refers to SNOMED CT component which can be- {@link Concept}, {@link Description} or
 * {@link Relationship}. Every component has a unique id and it derives its other attributes
 * from its parent class VersionedComponent.
 *
 */
public abstract class Component extends VersionedComponent {
    
    /**
	 * A permanent unique numeric identifier which does not convey any information about the meaning
	 * or nature of the Component.
	 */
    protected String id;

    /**
     * Default constructor
     */
    protected Component() {    
    }

    /**
     * Parameterized constructor
     * @param id
     * 			 	 {@link Component} represents the Component id.
     * @param effectiveTime
     * 			 	 represents Effective time associated with that component.
     * @param activeStatus
     * 				 represents the status of the component(0 for inactive and 1 for active).
     * @param moduleId
     * 				 represents the module Id.
     */
    protected Component(String id, Date effectiveTime, Byte activeStatus, String moduleId) {
        super(effectiveTime, activeStatus, moduleId);
        this.id = id;
    }

    /**
	 * Returns the component identifier.
	 * @return id of the component.
	 */
    public String getId() {
        return id;
    }

    /**
	 * Returns the component identifier.
	 * @return id of the component.
	 */
    public void setId(String id) {
        this.id = id;
    }

}
