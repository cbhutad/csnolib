package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumModifier {
    
    UNIVERSAL_RESTRICTION("900000000000452009"),
	EXISTENTIAL_RESTRICTION("900000000000451002");
	
	private static final Map<String, EnumModifier> lookup = new ConcurrentHashMap<String, EnumModifier>();
    
    static {
        for (EnumModifier d : EnumModifier.values())
            lookup.put(d.getValue(), d);
    }
    	
	private String value;
	
	private EnumModifier( String value) {
		this.value = value;
	}
	
	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
 
	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumModifier} enumeration for given string value
	 */
	public static EnumModifier getEnum(String val) 
	{
		return lookup.get(val);
	}

}
