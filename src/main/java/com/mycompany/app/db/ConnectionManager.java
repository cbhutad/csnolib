package com.mycompany.app.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mycompany.app.commons.Constants;
import com.mycompany.app.util.LoggerUtility;
import com.mycompany.app.util.PropertyReader;

public class ConnectionManager {
    
    private static final String jdbcClassName = "org.h2.Driver";
    private static final String userName = "csno";
    private static final String userPassword = "csno";
    private static Connection conn;
    private static final Logger logger = LogManager.getLogger(ConnectionManager.class);

    static {
        conn = ConnectionManager.getConnection();
    }

    private ConnectionManager() {}

    /**
	 * returns singleton database connection object.
	 * @return instance of {@link Connection}.
	 */
    public static Connection getConnection() {
        if(conn != null) {
            try {
                if(conn.isClosed()) {
                    conn = getConnectionInstance();
                }
            } catch (SQLException e) {
                logger.error(LoggerUtility.Exception, "Connection object is already closed");
                logger.error(LoggerUtility.Exception, LoggerUtility.createErrorMessage(e));
            }
        } else {
            conn = getConnectionInstance();
        }
        return conn;
    }

    /**
	 * returns new database connection object.
	 * @return instance of {@link Connection}.
	 */
    private static Connection getConnectionInstance() {
        try {
            PropertyReader.loadSystemProperties();
            Class.forName(jdbcClassName);
            conn = DriverManager.getConnection("jdbc:h2:" + PropertyReader.getProperty(Constants.DATA_DIR) + "/snomedct2;LOG=0;CACHE_SIZE=65536;LOCK_MODE=0;UNDO_LOG=0",userName,userPassword);
        } catch (ClassNotFoundException | SQLException e) {
            if(e instanceof SQLException) {
                SQLException ex = (SQLException)e;
                logger.info(LoggerUtility.CONSOLE, "SQLException: {}", ex.getMessage());
                logger.info(LoggerUtility.CONSOLE, "SQLState: {}", ex.getSQLState());
                logger.info(LoggerUtility.CONSOLE, "VendorError: {}", ex.getErrorCode());
            }
            logger.error(LoggerUtility.Exception, LoggerUtility.createErrorMessage(e));
        }
        return conn;
    }
}
