package com.mycompany.app.db;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Enum.EnumDesc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.management.relation.Relation;

//import org.h2.tools.RunScript;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.RunScript;

import com.mycompany.app.agents.SNOMEDAgent;
import com.mycompany.app.commons.Constants;
import com.mycompany.app.commons.DBSchemaMapping;
import com.mycompany.app.commons.EnumAcceptabilityId;
import com.mycompany.app.commons.EnumCaseSignificance;
import com.mycompany.app.commons.EnumCharacteristicType;
import com.mycompany.app.commons.EnumComponent;
import com.mycompany.app.commons.EnumDefStatus;
import com.mycompany.app.commons.EnumDescType;
import com.mycompany.app.commons.EnumDirection;
import com.mycompany.app.commons.EnumLangRefset;
import com.mycompany.app.commons.EnumModifier;
import com.mycompany.app.commons.EnumRelationshipType;
import com.mycompany.app.commons.EnumSemanticTag;
import com.mycompany.app.commons.EnumState;
import com.mycompany.app.model.Component;
import com.mycompany.app.model.ComponentFactory;
import com.mycompany.app.model.CompositeDescription;
import com.mycompany.app.model.Concept;
import com.mycompany.app.model.Description;
import com.mycompany.app.model.Relationship;
import com.mycompany.app.util.LoggerUtility;
import com.mycompany.app.util.SemanticTagBuilder;

/**
 * Establishes a connection to fetch data from SNOMED CT database.
 */
public class QueryManager {
    
    private static final String CREATING_TABLE = "Creating {} table";
    private static final String DELETING_RECORDS_FROM_TABLE = "Deleting records from {} table";
    private static final String NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9 = "',null,'charset=UTF-8 fieldDelimiter=fieldSeperator='||chr(9)";
    private static final String SORTED_SELECT_FROM_CSVREAD = "SELECT * FROM CSVREAD('";
    private static final String SELECT_FROM_CSVREAD = "SELECT * FROM CSVREAD('";
    private static final String DELETE_FROM = "delete from ";
    private static final String INSERT_INTO = "insert into ";
    private static final String MERGE_INTO = "merge into ";

    /**
     * SNOMEDAgent object
     */
    SNOMEDAgent snomedAgent = new SNOMEDAgent();
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
    private static final Logger logger = LogManager.getLogger(QueryManager.class);

    /**
     * Retrieves the semantic Tag/ Heirarchy for a particular Concept Id.
     * @param conceptId
     * @param typeId
     * @param term
     * @return {@link String}
     * @throws SQLException
     */
    public String getHeirarchy(String conceptId, String typeId, String term) throws SQLException {
        String strSemanticTag = "";
        String strTypeId = "";

        if(typeId.equalsIgnoreCase(EnumDescType.FULLY_SPECIFIED_NAME.getValue()) && term.contains("(") && term.contains(")")) {
            String strST = SemanticTagBuilder.buildSemanticTagFromString(term);
            if(null == strST) {
                strSemanticTag = EnumSemanticTag.SITUATION_WITH_EXPLICIT_CONTENT.getValue();
            }
        } else {
            Connection conn = ConnectionManager.getConnection();
            StringBuilder query = new StringBuilder();
            query.append("select " + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.DESCRIPTION_TYPE_ID + " from  " + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + conceptId + " and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_TERM + " like '%)%' and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1;");
            Statement statement = conn.createStatement();
            ResultSet result = statement.executeQuery(query.toString());
            if(result != null) {
                while(result.next()) {
                    term = result.getString(1);
                    strTypeId = result.getString(2);
                    if(strTypeId.equalsIgnoreCase((EnumDescType.FULLY_SPECIFIED_NAME.getValue())) && term.contains("(") && term.contains(")")) {
                        String strST = SemanticTagBuilder.buildSemanticTagFromString(term);
                        if(null == strST) {
                            strSemanticTag = EnumSemanticTag.SITUATION_WITH_EXPLICIT_CONTENT.getValue();
                            break;
                        }
                    }
                }
            }
            statement.close();
        }
        return strSemanticTag;
    }

    /**
     * Populate a SNOMED CT {@link Concept} associated for a given concept identifier.
     * @param id
     * @param concept
     * @throws SQLException
     */
    public void populateConcept(String id, Concept concept) throws SQLException, ParseException {
        if(null == concept || !id.matches("[0-9]*"))
            return;

        Connection conn = ConnectionManager.getConnection();
        StringBuilder query = new StringBuilder();
        query.append("select " + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.CONCEPT_ACTIVE + "," + DBSchemaMapping.CONCEPT_DEFINITION_STATUS + "," + DBSchemaMapping.CONCEPT_EFFECTIVE_TIME + "," + DBSchemaMapping.CONCEPT_MODULE_ID + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.CONCEPT_ID + " and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + id + " and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1;");
        Statement statment = conn.createStatement();
        ResultSet result = statment.executeQuery(query.toString());
        if(null != result) {
            if(result.next()) {
                concept.setId(id);
                concept.setFullySpecifiedName(result.getString(1));
                concept.setActiveStatus(result.getByte(2));
                concept.setDefinitionStatusId(EnumDefStatus.getEnum(result.getString(3)));
                concept.setEffectiveTime(parseToDate(result.getString(4)));
                concept.setModuleId(result.getString(5));
            }
        }
        statment.close();
    }

    /**
     * Returns list of {@link Description}s associated for a given concept Identifier and which is other than FSN.
     * @param conceptId
     * @return List of {@link Description}s
     * @throws Exception
     */
    public List<Description> getDescriptions(String conceptId) throws SQLException, ParseException {
        List<Description> descriptions = new ArrayList<>();
        Connection conn = ConnectionManager.getConnection();
        String query = "select " + DBSchemaMapping.DESCRIPTION_ID + "," + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "," + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.DESCRIPTION_TYPE_ID + "," + DBSchemaMapping.DESCRIPTION_ACTIVE + "," + DBSchemaMapping.DESCRIPTION_LANGUAGE_CODE + "," + DBSchemaMapping.DESCRIPTION_EFFECTIVE_TIME + "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + "," + DBSchemaMapping.DESCRIPTION_CASE_SIGNIFICANCE_ID + " from " + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + conceptId + " and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.SYNONYM.getValue();
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(query);
        if(null != result) {
            while(result.next()) {
                ComponentFactory componentFactory = ComponentFactory.getInstance();
                Description description = (Description)componentFactory.getComponent(EnumComponent.DESCRIPTION, null);
                description.setId(result.getString(1));
                description.setConceptId(result.getString(2));
                description.setTerm(result.getString(3));
                description.setTypeId(EnumDescType.getEnum(result.getString(4)));
                description.setActiveStatus(result.getByte(5));
                description.setLanguageCode(result.getString(6));
                description.setEffectiveTime(parseToDate(result.getString(7)));
                description.setModuleId(result.getString(8));
                description.setCaseSignificanceId(EnumCaseSignificance.getEnum(result.getString(9)));
                descriptions.add(description);
            }
        }
        statement.close();
        return descriptions;
    }

    /**
	 * Returns list of {@link CompositeDescription}s
	 *   associated for a given concept identifier and which is other than FSN.
	 * @param conceptId
	 * 					SNOMED CT {@link Concept} identifier.
	 * @return List of {@link CompositeDescription}s .
	 * @throws SQLException if any error occurs while accessing database.
	 * @throws ParseException
	 */
    public List<CompositeDescription> getAllDescriptions(String conceptId, EnumLangRefset enumLangRefsetId) throws SQLException, ParseException {
        List<CompositeDescription> descriptions = new ArrayList<>();
        String query = "Select " + DBSchemaMapping.DESCRIPTION_ID + "," + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "," + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.DESCRIPTION_TYPE_ID + "," + DBSchemaMapping.DESCRIPTION_ACTIVE + "," + DBSchemaMapping.DESCRIPTION_LANGUAGE_CODE + "," + DBSchemaMapping.DESCRIPTION_EFFECTIVE_TIME + "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + "," + DBSchemaMapping.DESCRIPTION_CASE_SIGNIFICANCE_ID + "," + DBSchemaMapping.REFSETLANG_ACCEPTABILITY_ID + "," + DBSchemaMapping.CONCEPT_ACTIVE + "," + DBSchemaMapping.CONCEPT_DEFINITION_STATUS + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.REFSETLANG + "," + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + conceptId + " and " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.CONCEPT_ID + " and " + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + " and " + DBSchemaMapping.REFSETLANG_REFSET_ID + "=" + enumLangRefsetId.getId() + " and " + DBSchemaMapping.REFSETLANG_ACTIVE + "=1";
        Connection connection = ConnectionManager.getConnection();
        Statement statement = connection.createStatement();
        ResultSet result =  statement.executeQuery(query);

        if(null != result) {
            while(result.next()) {
                CompositeDescription description = new CompositeDescription();
                description.setId(result.getString(1));
                description.setConceptId(result.getString(2));
                description.setTerm(result.getString(3));
                description.setTypeId(EnumDescType.getEnum(result.getString(4)));
                description.setActiveStatus(result.getByte(5));
                description.setLanguageCode(result.getString(6));
                description.setEffectiveTime(parseToDate(result.getString(7)));
                description.setModuleId(result.getString(8));
                description.setCaseSignificanceId(EnumCaseSignificance.getEnum(result.getString(9)));
                description.setConceptState(result.getString(11));
                description.setDefinitionStatus(result.getString(12));
                
                String queryFsn = "Select " + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.CONCEPT_ACTIVE + "," + DBSchemaMapping.CONCEPT_DEFINITION_STATUS + "," + DBSchemaMapping.DESCRIPTION_MODULE_ID + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + conceptId + " and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1";

                Statement statementFSN = connection.createStatement();
                ResultSet rs = statementFSN.executeQuery(queryFsn);

                if(rs.next()) {
                    description.setConceptFsn(rs.getString(1));
                    description.setHierarchy(SemanticTagBuilder.buildSemanticTagFromString(rs.getString(1)));
                }
                if(EnumAcceptabilityId.PREFERRED.getValue().equalsIgnoreCase(result.getString(10)))
                    description.setIsPreferredTerm("1");
                else
                    description.setIsPreferredTerm("0");

                descriptions.add(description);
            }
        }

        statement.close();
        return descriptions;
    }

    /**
	 * Searches for concepts in the SNOMED CT repository for the
	 *  given search term.
	 * @param searchTerm
	 * 					{@link String} term/text to be searched .
	 * @return Set of {@link Concept}s.
	 * @throws SQLException if any error occurs while accessing database.
	 */
    public Set<Concept> searchConcepts(String searchTerm) throws SQLException {
        Set<Concept> concepts = new HashSet<>();
        Connection connection = ConnectionManager.getConnection();

        String query = "Select * from " + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.CONCEPT_ID + " in ( Select " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + " from " + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.DESCRIPTION_TERM + " like '%" + searchTerm + "%') and " + DBSchemaMapping.CONCEPT_ACTIVE + "=1;";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        
        if(null != resultSet) {
            while(resultSet.next()) {
                ComponentFactory componentFactory = ComponentFactory.getInstance();
                Concept concept = (Concept) componentFactory.getComponent(EnumComponent.CONCEPT, null);
                concept.setId(resultSet.getString(1));
                concepts.add(concept);
            }
        }
        statement.close();
        return concepts;
    }

    /**
	 * Returns unique Inward/Outward latest relationships with active status.
	 * @param ConceptId
	 * 					identifier of the {@link Concept}.
	 * @param enumRelationshipType
	 * 					specifies the type of relationship.
	 * @param direction
	 * 					specifies the direction(source / destination) {@link EnumDirection}.
	 * @return Set of {@link Relationship}s .
	 * @throws SQLException if any error occurs while accessing database.
	 * @throws ParseException
	 */ 
     public List<Relationship> getRelationships(String conceptId, String relationshipType, EnumDirection direction) throws SQLException {
        List<Relationship> relationships = new ArrayList<>();
        Connection connection = ConnectionManager.getConnection();
        String query;

        if(relationshipType.toLowerCase().equals(EnumRelationshipType.ALL.getValue())) {
            query = "Select * from " + DBSchemaMapping.RELATIONSHIP + " where " + DBSchemaMapping.RELATIONSHIP_SOURCE_ID + "=" + conceptId + " and " + DBSchemaMapping.RELATIONSHIP_ACTIVE + "=1;";
        } else if(direction == EnumDirection.DESTINATION) {
            query = "Select * from " + DBSchemaMapping.RELATIONSHIP + " where " + DBSchemaMapping.RELATIONSHIP_TYPEID + "=" + relationshipType + " and " + DBSchemaMapping.RELATIONSHIP_DESTINATION_ID + "=" + conceptId + " and " + DBSchemaMapping.RELATIONSHIP_ACTIVE + "=1;";
        } else {
            query = "Select * from " + DBSchemaMapping.RELATIONSHIP + " where " + DBSchemaMapping.RELATIONSHIP_TYPEID + "=" + relationshipType + " and " + DBSchemaMapping.RELATIONSHIP_SOURCE_ID + "=" + conceptId + " and " + DBSchemaMapping.RELATIONSHIP_ACTIVE + "=1;";
        }
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        if(null != resultSet) {
            while(resultSet.next()) {
                ComponentFactory componentFactory = ComponentFactory.getInstance();
                Relationship relationship = (Relationship) componentFactory.getComponent(EnumComponent.RELATIONSHIP, null);
                relationship.setSourceConcept(snomedAgent.getConcept(resultSet.getString(5).trim()));
                Concept destinationConcept = snomedAgent.getConcept(resultSet.getString(6).trim());
                relationship.setDestinationConcept(destinationConcept);
                relationship.setType(snomedAgent.getConcept(resultSet.getString(8).trim()));
                relationship.setCharacteristicTypeId(EnumCharacteristicType.getEnum(resultSet.getString(9)));
                relationship.setModifierId(EnumModifier.getEnum(resultSet.getString(10)));
                relationship.setActiveStatus(resultSet.getByte(3));
                relationship.setEffectiveTime(parseToDate(resultSet.getString(2)));
                relationship.setModuleId(resultSet.getString(4));
                relationship.setRelationshipGroup(resultSet.getInt(7));

                relationships.add(relationship);
            }
        }
        statement.close();
        return relationships;
    }

    /**
	 * This function return the list of parent Concepts for a particular concept.
	 * @param ConceptId
	 * 					 the id of the SNOMED CT Concept whose parent concepts
	 * 					 are to be retrieved.
	 * @return {@link Set} of {@link Concept}s that are parents of the specified concept.
	 * @throws SQLException
	 */
    public Set<Concept> getParents(String conceptId) throws SQLException {
        Set<Concept> concepts = new HashSet<>();
        Connection connection = ConnectionManager.getConnection();
        
        String query = "Select " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "," + DBSchemaMapping.DESCRIPTION_TERM + "," + DBSchemaMapping.CONCEPT_ACTIVE + "," + DBSchemaMapping.CONCEPT_MODULE_ID + "," + DBSchemaMapping.CONCEPT_DEFINITION_STATUS + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.CONCEPT + "," + DBSchemaMapping.RELATIONSHIP + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.CONCEPT_ID + " and " + DBSchemaMapping.CONCEPT_ID + "=" + DBSchemaMapping.RELATIONSHIP_DESTINATION_ID + " and " + DBSchemaMapping.RELATIONSHIP_TYPEID + "=" + EnumRelationshipType.IS_A.getId() + " and " + DBSchemaMapping.RELATIONSHIP_SOURCE_ID + "=" + conceptId + " and " + DBSchemaMapping.RELATIONSHIP_ACTIVE + "=1 and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1;";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        if(resultSet != null) {
            while(resultSet.next()) {
                ComponentFactory componentFactory = ComponentFactory.getInstance();
                Concept concept = (Concept) componentFactory.getComponent(EnumComponent.CONCEPT, null);
                concept.setId(resultSet.getString(1));
                concept.setFullySpecifiedName(resultSet.getString(2));
                concept.setActiveStatus(resultSet.getByte(3));
                concept.setModuleId(resultSet.getString(4));
                concept.setDefinitionStatusId(EnumDefStatus.getEnum(resultSet.getString(5)));
                concepts.add(concept);
            }
        }
        statement.close();
        return concepts;
    }

    /**
	 * This function returns list of child Concepts for a particular concept.
	 * @param ConceptId
	 * 					 the id of the SNOMED CT Concept whose children are to
	 * 					 be retrieved.
	 * @return {@link Set} of {@link Concept}s that are children of the specified concept.
	 * @throws SQLException
	 */
    public Set<Concept> getChildren(String conceptId) throws SQLException {
        Set<Concept> concepts = new HashSet<>();
        Connection connection = ConnectionManager.getConnection();

        String query = "Select " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "," + DBSchemaMapping.DESCRIPTION_TERM +  "," + DBSchemaMapping.CONCEPT_ACTIVE + "," + DBSchemaMapping.CONCEPT_MODULE_ID + " from " + DBSchemaMapping.DESCRIPTION + "," + DBSchemaMapping.CONCEPT + "," + DBSchemaMapping.RELATIONSHIP + " where " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + DBSchemaMapping.CONCEPT_ID + " and " + DBSchemaMapping.CONCEPT_ID + "=" + DBSchemaMapping.RELATIONSHIP_SOURCE_ID + " and " + DBSchemaMapping.RELATIONSHIP_TYPEID + "=" + EnumRelationshipType.IS_A.getId() + " and " + DBSchemaMapping.RELATIONSHIP_DESTINATION_ID + "=" + conceptId + " and " + DBSchemaMapping.RELATIONSHIP_ACTIVE + "=1 and " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.FULLY_SPECIFIED_NAME.getValue() + " and " + DBSchemaMapping.DESCRIPTION_ACTIVE + "=1;";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);

        if(resultSet != null) {
            while(resultSet.next()) {
                ComponentFactory componentFactory = ComponentFactory.getInstance();
                Concept concept = (Concept) componentFactory.getComponent(EnumComponent.CONCEPT, null);
                concept.setId(resultSet.getString(1));
                concept.setFullySpecifiedName(resultSet.getString(2));
                concept.setActiveStatus(resultSet.getByte(3));
                concept.setDefinitionStatusId(EnumDefStatus.getEnum(resultSet.getString(4)));
                concept.setModuleId(resultSet.getString(5));
                concepts.add(concept);
            }
        }
        statement.close();
        return concepts;
    }

    /**
	 * Returns list of preferred names in US refset associated with
	 * corresponding {@link Concept} identifier.
	 * @param enumLangRefset specifies the Language Refset {@link EnumLangRefset} }.
	 * @param conceptId identifier of the {@link Concept}.
	 * @return Preferred name of {@link Concept}.
	 * @throws SQLException
	 */
    public String getPreferredName(String concpetId, EnumLangRefset refset) throws SQLException {
        String query = "Select " + DBSchemaMapping.DESCRIPTION_TERM + " from (" + DBSchemaMapping.DESCRIPTION + " join " + DBSchemaMapping.REFSETLANG + " on (" + DBSchemaMapping.DESCRIPTION_ID + "=" + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + " )) where (( " + DBSchemaMapping.DESCRIPTION_ACTIVE + " =1 ) and ( " + DBSchemaMapping.DESCRIPTION_TYPE_ID + "=" + EnumDescType.SYNONYM.getValue() + ") and ( " + DBSchemaMapping.DESCRIPTION_CONCEPT_ID + "=" + concpetId + " and " + DBSchemaMapping.REFSETLANG_REFSET_ID + "= '" + refset.getId() + "') and ( "  + DBSchemaMapping.REFSETLANG_ACTIVE + " = 1) and ( " + DBSchemaMapping.REFSETLANG_ACCEPTABILITY_ID + "=" + EnumAcceptabilityId.PREFERRED.getValue() + "))";
        Connection conn = ConnectionManager.getConnection();
        PreparedStatement ps = conn.prepareStatement(query);
        ResultSet result = ps.executeQuery();
        result.next();
        String preferredName = result.getString(1);
        ps.close();
        return preferredName;
    }

    
	/**
	 * Returns a {@link List} of all Descendants {@link Concept}s  for given identifier.
	 * @param parentId
	 * 				{@link Concept} identifier.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getAllDescendantConcepts(String parentId) throws SQLException {
		Set<Concept> descendantConcepts = new HashSet<>();

		String query = "Select " + DBSchemaMapping.TRANSCLOSURE_CHILD_ID + " from " + DBSchemaMapping.TRANSCLOSURE + " where " + DBSchemaMapping.TRANSCLOSURE_PARENT_ID + "=" + parentId;
		Connection conn = ConnectionManager.getConnection();

		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result = ps.executeQuery();

		if(null != result)
		{
			while(result.next())
			{
				ComponentFactory componentFactory=ComponentFactory.getInstance();
				Concept concept = (Concept)componentFactory.getComponent(EnumComponent.CONCEPT, result.getString(1));
				descendantConcepts.add(concept);
			}
		}
		ps.close();
		return descendantConcepts;
	}

    /**
	 * Returns a {@link Set}} of all Refset Members {@link Concept}s  for given refset identifier.
	 * @param refsetId
	 * 				{@link Concept} identifier.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getAllRefsetMembers(String refsetId) throws SQLException {
		Set<Concept> refesetMembers = new HashSet<>();

		String query = "Select "+ DBSchemaMapping.REFSET_REFERENCED_COMPONENT_ID + " from " + DBSchemaMapping.REFSET + " where " + DBSchemaMapping.REFSET_REFSET_ID + "= '" + refsetId + "'";
		Connection conn = ConnectionManager.getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result = ps.executeQuery();

		if(null != result)
		{
			while(result.next())
			{
				ComponentFactory componentFactory=ComponentFactory.getInstance();
				Concept concept = (Concept)componentFactory.getComponent(EnumComponent.CONCEPT, result.getString(1));
				refesetMembers.add(concept);
			}
		}
		ps.close();

		return refesetMembers;
	}


    /**
	 * Returns a {@link Set} of all Refset {@link Concept}s.
	 * @return Set of {@link Concept}s.
	 */
	public Set<Concept> getAllRefsets() throws SQLException {
		Set<Concept> refsets = new HashSet<>();

		String query = "Select distinct( " + DBSchemaMapping.REFSET_REFSET_ID + ") from " + DBSchemaMapping.REFSET;
		Connection conn = ConnectionManager.getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result = ps.executeQuery();

		if(null != result)
		{
			PreparedStatement cps = null;
			while(result.next())
			{
				ComponentFactory componentFactory = ComponentFactory.getInstance();

				Concept concept = null;

				if (result.getString(1).matches("[0-9]*")){
				String conceptQuery = "Select * from "+ DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.CONCEPT_ID + "=" + result.getString(1);
				cps = conn.prepareStatement(conceptQuery);
				ResultSet conceptResult = cps.executeQuery();

				if(conceptResult.next())
					concept = (Concept)componentFactory.getComponent(EnumComponent.CONCEPT, result.getString(1));

				else{
					concept = (Concept)componentFactory.getComponent(EnumComponent.CONCEPT, null);
					concept.setId(result.getString(1));
					concept.setFullySpecifiedName("Beta version of the reference set");
					}
				cps.close();
				}
				else{
					concept = (Concept)componentFactory.getComponent(EnumComponent.CONCEPT, null);
					concept.setId(result.getString(1));
					concept.setFullySpecifiedName("Beta version of the reference set");
					}


				refsets.add(concept);
			}
		}
		ps.close();
		return refsets;
	}


    /**
	 * This function loads the closure table in the database.
	 * @param ct_file
	 * 					File associated with the closure table.
	 * @throws SQLException
	 * @throws IOException
	 */

	public void loadClosureTable(File ct_file) throws SQLException, IOException	{

	    String	query= ("insert into sct2_relationships_tc SELECT * FROM CSVREAD('" + ct_file.getCanonicalPath().replace("\\","/") + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

		Connection conn = ConnectionManager.getConnection();
		PreparedStatement ps = conn.prepareStatement(query);

		ps.executeUpdate();
		ps.close();
	}
    
    /**
	 * Returns acceptability Id for specified description id.
	 * acceptability id indicates whether particular description is Preferred Term or not.
	 * @param descriptionId
	 * 						{@link String} identifier of the {@link Description}.
	 * @return {@link String} value that specifies the acceptability id.
	 * @throws SQLException
	 */
	public String getAcceptabilityId(String descriptionId) throws SQLException {
		//	SELECT acceptabilityId   FROM  der2_refset_lang
		//	WHERE referencedComponentId = 1480803010 ;
		String query = "select "+ DBSchemaMapping.REFSETLANG_ACCEPTABILITY_ID + " from  " + DBSchemaMapping.REFSETLANG  + " where "  + DBSchemaMapping.REFSETLANG_REFERENCED_COMPONENT_ID + "=" + descriptionId ;

		Connection conn = ConnectionManager.getConnection();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet result =	ps.executeQuery();
		result.next();
		String acceptabilityId=result.getString(1);

		ps.close();
		return acceptabilityId;
	}

    /**
	 * This method will create SNOMED CT database
	 * @param conceptFile file path for Concept File
	 * @param descriptionFile file path for Description File
	 * @param relationshipFile file path for Relationship File
	 * @param refsetLangFile file path for Language Refset File
	 * @throws SQLException
	 */
     public void createSNOMEDdb(String conceptFile, String descriptionFile, String relationshipFile, String refsetLangFile,String textdefinitionFile,String concreteRelationshipFile) throws SQLException {
		Connection conn =null;
		Statement stm = null;
		try{
			conn = ConnectionManager.getConnection();
			stm = conn.createStatement();

			RunScript.execute(conn, new InputStreamReader(QueryManager.class.getClassLoader().getResourceAsStream("SNOMEDDBH2Script.sql")));
			logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.CONCEPT);
			stm.execute(INSERT_INTO + DBSchemaMapping.CONCEPT + SORTED_SELECT_FROM_CSVREAD + conceptFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

			logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.DESCRIPTION);
			stm.execute(INSERT_INTO + DBSchemaMapping.DESCRIPTION + SORTED_SELECT_FROM_CSVREAD + descriptionFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

			logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.RELATIONSHIP);
			stm.execute(INSERT_INTO + DBSchemaMapping.RELATIONSHIP + SORTED_SELECT_FROM_CSVREAD + relationshipFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

			if(concreteRelationshipFile!=null) {
				logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.CONCRETE_RELATIONSHIP);
				stm.execute(INSERT_INTO + DBSchemaMapping.CONCRETE_RELATIONSHIP + SORTED_SELECT_FROM_CSVREAD + concreteRelationshipFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);
			}

			logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.REFSETLANG);
			stm.execute(INSERT_INTO + DBSchemaMapping.REFSETLANG + SORTED_SELECT_FROM_CSVREAD + refsetLangFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

			logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.TEXTDEFINITION);
			stm.execute(INSERT_INTO + DBSchemaMapping.TEXTDEFINITION + SORTED_SELECT_FROM_CSVREAD + textdefinitionFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);
		
        } finally {
			if(stm!=null)
				stm.close();
			if(conn!=null)
				conn.close();
			}
	}

    /**
	 * This method will create SNOMED CT extension database
	 * @param conceptFile file path for Concept File
	 * @param descriptionFile file path for Description File
	 * @param relationshipFile file path for Relationship File
	 * @param refsetLangFile file path for Language Refset File
	 * @param Set of moduleId
	 * @throws SQLException
	 */
	public void createExtensiondb(Set<String> moduleIdSet, String conceptFile, String descriptionFile, String relationshipFile, String refsetLangFile, String concreteRelationshipFile) throws SQLException{
		Connection conn =null;
		Statement stm = null;
	    try{
            conn = ConnectionManager.getConnection();
            stm = conn.createStatement();

			ResultSet rst= stm.executeQuery("select count(*) from "+DBSchemaMapping.CONCEPT + " Where " + DBSchemaMapping.CONCEPT_ID + " like '%00_'");
			if(!(rst.next() && rst.getInt(1)>0))
				return;
			//Delete module id based data
			//Except INDIA NRC module
			String moduleId="";
			for(String element: moduleIdSet){
				if( ! Constants.INDIA_MODULE_ID.equalsIgnoreCase(element) ) {
					if(moduleId=="")
						moduleId=element;
					else
						moduleId=moduleId+","+element;
				}
			}

			//this should be done firstly
			//Deleting / truncating old TC table
			stm.execute("TRUNCATE TABLE " + DBSchemaMapping.TRANSCLOSURE);
			logger.info(LoggerUtility.ConsoleWithLog, "Truncated old {} table", DBSchemaMapping.TRANSCLOSURE);
			//Delete module id based data
			stm.execute(DELETE_FROM + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.CONCEPT_MODULE_ID + " in (" + moduleId + ")");
			logger.info(LoggerUtility.ConsoleWithLog, DELETING_RECORDS_FROM_TABLE ,DBSchemaMapping.CONCEPT);
			
            stm.execute(DELETE_FROM + DBSchemaMapping.DESCRIPTION + " where " + DBSchemaMapping.DESCRIPTION_MODULE_ID + " in (" + moduleId + ")");
			logger.info(LoggerUtility.ConsoleWithLog, DELETING_RECORDS_FROM_TABLE, DBSchemaMapping.DESCRIPTION);
			
            stm.execute(DELETE_FROM + DBSchemaMapping.RELATIONSHIP + " where " + DBSchemaMapping.RELATIONSHIP_MODULE_ID + " in (" + moduleId + ")");
			logger.info(LoggerUtility.ConsoleWithLog, DELETING_RECORDS_FROM_TABLE ,DBSchemaMapping.RELATIONSHIP);
			
            if(concreteRelationshipFile!=null) {
				stm.execute(DELETE_FROM + DBSchemaMapping.CONCRETE_RELATIONSHIP + " where " + DBSchemaMapping.CONCRETE_RELATIONSHIP_MODULE_ID + " in (" + moduleId + ")");
				logger.info(LoggerUtility.ConsoleWithLog, DELETING_RECORDS_FROM_TABLE ,DBSchemaMapping.CONCRETE_RELATIONSHIP);
			}

			stm.execute(DELETE_FROM + DBSchemaMapping.REFSETLANG + " where " + DBSchemaMapping.REFSETLANG_MODULE_ID + " in (" + moduleId + ")");
			logger.info(LoggerUtility.ConsoleWithLog, DELETING_RECORDS_FROM_TABLE, DBSchemaMapping.REFSETLANG);

			logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.CONCEPT);
			stm.execute(MERGE_INTO + DBSchemaMapping.CONCEPT + SELECT_FROM_CSVREAD + conceptFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

			logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.DESCRIPTION);
			stm.executeUpdate(MERGE_INTO + DBSchemaMapping.DESCRIPTION + SELECT_FROM_CSVREAD + descriptionFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

			logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.RELATIONSHIP);
			Constants.RELATIONSHIP_EXTENSION_RECORDS_COUNT = stm.executeUpdate(MERGE_INTO + DBSchemaMapping.RELATIONSHIP + SELECT_FROM_CSVREAD + relationshipFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

			if(concreteRelationshipFile!=null) {
                logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.CONCRETE_RELATIONSHIP);
                Constants.CONCRETE_RELATIONSHIP_EXTENSION_RECORDS_COUNT = stm.executeUpdate(INSERT_INTO + DBSchemaMapping.CONCRETE_RELATIONSHIP + SELECT_FROM_CSVREAD + concreteRelationshipFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);
			}
			
            logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE, DBSchemaMapping.REFSETLANG);
			stm.executeUpdate(MERGE_INTO + DBSchemaMapping.REFSETLANG + SELECT_FROM_CSVREAD + refsetLangFile + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

		} finally {
            if(stm!=null)
                stm.close();
            if(conn!=null)
                conn.close();
		}

	}

    /**
	 * This method will create database for Refset
	 * @param refsetId
	 * @param refFilePath
	 * @throws SQLException
	 */
	public void createRefsetdb(String refsetId, String refFilePath, Set<String> moduleIdSet) throws SQLException{
		Connection conn =null;
		Statement stm = null;
	try{
		conn = ConnectionManager.getConnection();
		stm = conn.createStatement();
		logger.info(LoggerUtility.ConsoleWithLog, CREATING_TABLE,refsetId);

				for(String moduleid:moduleIdSet)
					stm.execute(DELETE_FROM + DBSchemaMapping.REFSET+ " where " + DBSchemaMapping.REFSET_REFSET_ID + " = '" + refsetId + "' and " + DBSchemaMapping.REFSET_MODULE_ID + " = '" + moduleid + "' ;");
				stm.execute(INSERT_INTO + DBSchemaMapping.REFSET + SELECT_FROM_CSVREAD + refFilePath + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);

		} finally {
            if(stm!=null)
                stm.close();
            if(conn!=null)
                conn.close();
		}
	}

    /**
	 * This method will create database for cRefset
	 * @param refsetId
	 * @param refFilePath
	  * @param moduleIdSet
	 * @throws SQLException
	 */
	public void createcRefsetdb(String refsetId, String refFilePath, Set<String> moduleIdSet) throws SQLException{
		Connection	conn=ConnectionManager.getConnection();
		Statement	stm=conn.createStatement();
			for(String moduleid:moduleIdSet){
				if(!moduleid.equalsIgnoreCase("900000000000207008"))
					stm.execute(DELETE_FROM + DBSchemaMapping.cREFSET + " where " + DBSchemaMapping.cREFSET_REFSET_ID + " = '" + refsetId + "' and " + DBSchemaMapping.cREFSET_MODULE_ID + " = '" + moduleid + "' and " + DBSchemaMapping.cREFSET_REFSET_ID + " = " + refsetId + ";");
				else{
					if(refsetId.equalsIgnoreCase("0"))
						stm.execute(DELETE_FROM + DBSchemaMapping.cREFSET + ";");
				}
			}

			stm.execute(INSERT_INTO + DBSchemaMapping.cREFSET + SELECT_FROM_CSVREAD + refFilePath + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);
    }

    /**
	 * This method will create database for simpleMap
	 * @param moduleId
	 * @param simpleMapFilePath
	 * @throws SQLException
	 */
	public boolean createSimpleMapdb(String moduleId, String simpleMapFilePath) throws SQLException{
		Connection conn =null;
		Statement stm = null;
        try{
            conn = ConnectionManager.getConnection();
            stm = conn.createStatement();
            logger.info(LoggerUtility.ConsoleWithLog, "Creating simple map table");
            ResultSet resultSet=stm.executeQuery("SELECT * FROM " + DBSchemaMapping.CONCEPT + " where " + DBSchemaMapping.CONCEPT_ID + " = '" + moduleId + "';");
            
            if(resultSet.next()==false) {
                return false;
            }
            
            stm.execute(DELETE_FROM + DBSchemaMapping.SIMPLEMAP + " where " + DBSchemaMapping.SIMPLEMAP_MODULE_ID + "= '" + moduleId + "';");
            Constants.SIMPLEMAP_RECORDS_COUNT=stm.executeUpdate(INSERT_INTO + DBSchemaMapping.SIMPLEMAP + SELECT_FROM_CSVREAD + simpleMapFilePath + NULL_CHARSET_UTF_8_FIELD_DELIMITER_FIELD_SEPARATOR_CHR_9);
            return true;
        } finally {
            if(stm!=null)
                stm.close();
            if(conn!=null)
                conn.close();
		}
	}

    /**
     * This method will export relationship table into a tsv file (for TC table creation)
     * @throws SQLException
     * @throws IOException
     */
    public String exportRelationshipTableAndDeleteTCTable() throws SQLException , IOException{
        Connection conn =null;
        Statement stm = null;
        String filePath = "";
        try{
            conn = ConnectionManager.getConnection();
            stm = conn.createStatement();
            File relationshipTemp = File.createTempFile("relationship_rf2_", ".txt");
            filePath = relationshipTemp.getAbsolutePath();
            ResultSet resultSet=stm.executeQuery("CALL CSVWRITE('" + filePath + "', 'SELECT * FROM " + DBSchemaMapping.RELATIONSHIP + "', 'charset=UTF-8 fieldSeparator=' || CHAR(9));");
            if(resultSet.next()==false) {
                return "Error";
            }
            logger.info(LoggerUtility.ConsoleWithLog, "Exported {} table to temporary file ({} records)", DBSchemaMapping.RELATIONSHIP, resultSet.getLong(1));
            return filePath;
        } finally {
            if(stm!=null)
                stm.close();
            if(conn!=null)
                conn.close();
        }
    }

    /**
     * This method will delete temporary relationship file created for TC table generation
     */
    public void deleteRelationshipTempFile(String filePath) {
        File relationshipTemp = new File(filePath);
        boolean deleteStatus = relationshipTemp.delete();
        if( deleteStatus )
            logger.info(LoggerUtility.ConsoleWithLog, "Deleted temporary relationship file");
    }

    private Date parseToDate(String str) throws ParseException {
        synchronized(this) {
            return simpleDateFormat.parse(str);
        }
    }

}
