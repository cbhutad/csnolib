package com.mycompany.app.model;

/**
 * class RefSetMember derived from {@link Derivative}
 * Each Refset shall be added as a separate class that shall inherit this class 
 *
 */
public class RefSetMember extends Derivative {
    
    /**
	 * Unique identifier for a Refset
	 */
	protected String refSetId;
	
    /**
	 * Refers to id of a component. It is mostly a SNOMED CT Concept Id
	 */
	protected String referencedComponentId;
	
	protected String acceptabilityId;
	
	
	public String getAcceptabilityId() {
		return acceptabilityId;
	}
	public void setAcceptabilityId(String acceptabilityId) {
		this.acceptabilityId = acceptabilityId;
	}
	
    /**
	 * Returns id of the Refset
	 * @return id of a Refset 
	 */
	public String getRefSetId() {
		return refSetId;
	}
	/**
	 * Sets the Refset Id
	 * @param refSetId
	 * 				 Id of a Refset 
	 */
	public void setRefSetId(String refSetId) {
		this.refSetId = refSetId;
	}
	
    /**
	 * Returns the Id of the SNOMED CT Component related to the Refset member
	 * @return referenced Component Id
	 */
	public String getReferencedComponentId() {
		return referencedComponentId;
	}
	/**
	 * Sets the referencedComponentId
	 * @param referencedComponentId
	 * 								 id of the SNOMED CT Component 
	 * 	that is related to the Refset member
	 */
	public void setReferencedComponentId(String referencedComponentId) {
		this.referencedComponentId = referencedComponentId;
	}

}
