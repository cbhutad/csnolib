package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumComponent {
    
    CONCEPT("Concept"),
	DESCRIPTION("Description"),
	RELATIONSHIP("Relationship"),
	CONCRETERELATIONSHIP("ConcreteRelationship"),
	SIMPLEMAP("SimpleMapRefset");

	private static final Map<String, EnumComponent> lookup = new ConcurrentHashMap<String, EnumComponent>();
	
    static {
		for (EnumComponent d : EnumComponent.values())
			lookup.put(d.getValue(), d);
	}

	private String value;

	private EnumComponent( String value) {
		this.value = value;
	}

	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumComponent} enumeration for given string value
	 */
	public static EnumComponent getEnum(String val) {
		return lookup.get(val);
	}

}
