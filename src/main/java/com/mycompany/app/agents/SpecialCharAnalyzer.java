package com.mycompany.app.agents;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;

/**
 * An implementation of {@link Analyzer}
 * It is a custom analyzer which extends abstract class Analyzer
 */
class SpecialCharAnalyzer extends Analyzer {

    /**
     * createComponents method is overrided to user customized {@link Tokenizer}
     * @return {@link TokenStreamComponents}
     */
    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        Tokenizer whiteSpaceTokenizer = new WhitespaceTokenizer(); //explicitly excludes the non breaking spaces
        LowerCaseFilter lowerCaseFilterTokenStream = new LowerCaseFilter(whiteSpaceTokenizer); //Normalizes token text to lower case.
        return new TokenStreamComponents(whiteSpaceTokenizer, lowerCaseFilterTokenStream);
    }
     
}
