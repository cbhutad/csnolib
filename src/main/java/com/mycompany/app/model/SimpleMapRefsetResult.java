package com.mycompany.app.model;

public class SimpleMapRefsetResult {

    private Long conceptId;
    private String mapStatus;
    private SimpleMapRefsetGroup simpleMapRefsetGroup;
    
    /**
	 * Gets the concept id.
	 * @return
	 * SNOMED CT (conceptId)
	 */
    public Long getConceptId() {
        return conceptId;
    }

    /**
	 * Sets the concept id.
	 * @param conceptId
	 * SNOMED CT (conceptId)
	 */
    public void setConceptId(Long conceptId) {
        this.conceptId = conceptId;
    }

    /**
	 * Gets the mapping status.
	 * NO_MAP_FOUND, MAP_FOUND.
	 * @return
	 * map status
	 */
    public String getMapStatus() {
        return mapStatus;
    }

    /**
	 * Sets the mapping status.
	 * NO_MAP_FOUND, MAP_FOUND.
	 * @param mapStatus
	 * map status
	 */
    public void setMapStatus(String mapStatus) {
        this.mapStatus = mapStatus;
    }

    /**
	 * Gets the simple map group.
	 * @return
	 * list of simple map group.
	 */
    public SimpleMapRefsetGroup getSimpleMapRefsetGroup() {
        return simpleMapRefsetGroup;
    }

    /**
	 * Sets the simple map group.
	 * @param simpleMapRefsetGroup
	 * list of simple map group.
	 */
    public void setSimpleMapRefsetGroup(SimpleMapRefsetGroup simpleMapRefsetGroup) {
        this.simpleMapRefsetGroup = simpleMapRefsetGroup;
    }
    
}
