package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumDefStatus {
    
    PRIMITIVE("900000000000074008"),
	DEFINED("900000000000073002");

	private static final Map<String, EnumDefStatus> lookup = new ConcurrentHashMap<String, EnumDefStatus>();

    static {
        for (EnumDefStatus d : EnumDefStatus.values()){
            lookup.put(d.getValue(), d);
        }
    }

	private String value;

	private EnumDefStatus( String value) {
		this.value = value;
	}

	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumDefStatus} enumeration for given string value
	 */
	public static EnumDefStatus getEnum(String val) {
		return lookup.get(val);
	}

}
