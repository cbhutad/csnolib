package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumProperties {

    DATA_PATH(System.getProperty("user.dir")),	
	ERROR_LOG_PATH(System.getProperty("user.dir")),
	TOP_SUGGEST_RETURNLIMIT_VAL("50"),
	SUGGEST_RETURNLIMIT_VAL("1000"),
	SEARCH_RETURNLIMIT_VAL("200000"),
	INDEX_OFFSET_VAL("100000");
	
	private static final Map<String, EnumProperties> lookup = new ConcurrentHashMap<String, EnumProperties>();
	
    static {
        for (EnumProperties d : EnumProperties.values())
            lookup.put(d.getValue(), d);
    }
	
    private String value;
	
    private EnumProperties(String value) {
		this.value=value;
	}
	
	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value)
	{
		this.value = value;
	}

	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() 
	{
		return value;
	}
	
	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumProperties} enumeration for given string value
	 */
	public static EnumProperties getEnum(String val) 
	{
		return lookup.get(val);
	}

}
