package com.mycompany.app.model;

import java.util.Date;

public class SimpleMapRefset extends Component {
    
    /**
	 * Unique identifier for a Refset
	 */
	protected String refSetId;
	/**
	 * Refers to id of a component. It is mostly a SNOMED CT Concept Id
	 */
	protected Concept referencedComponent;
	/**
	 * Unique identifier for a SimpleMapRefset
	 */
	protected String mapTarget;

    /**
	 * Default constructor.
	 */
	protected SimpleMapRefset()	{
	}

    /**
	 * Parameterized constructor.
	 * @param id
	 * 			 Id of the particular SNOMED CT SimpleMapRefset.
	 * @param effectiveTime
	 * 			 Effective time associated with the SimpleMapRefset.
	 * @param activeStatus
	 * 			 Active status of the relationship (0 or 1).
	 * @param moduleId
	 * 			 represents module Id.
	 * @param referencedComponent
	 * 			 referencedComponent of the concept.
	 * @param refSetId
	 * 			 specifies the refSetId.
	 * @param mapTarget
	 * 			specifies the mapTarget.
	 */
    protected SimpleMapRefset(String id, Date effectiveTime, Byte activeStatus, String moduleId, String refsetId, Concept referencedComponent, String mapTarget) {
        super(id, effectiveTime, activeStatus, moduleId);
        this.refSetId = refsetId;
        this.referencedComponent = referencedComponent;
        this.mapTarget = mapTarget;
    }

     /**
	 * Returns the {@link Concept} identifier.
	 * @return id of that particular Concept.
	 */
    public String getId() {
        return super.getId();
    }

    /**
	 * Sets the Concept identifier.
	 * @param id
	 * 			 represents Concept identifier.
	 */
    public void setId(String id) {
        super.setId(id);
    }

    /**
	 * Returns the active status of the Concept.
	 * @return activeStatus of that concept.
	 */
    public Byte getActiveStatus() {
        return super.getActiveStatus();
    }

    /**
	 * Sets the active status of the Concept.
	 * @param activeStatus			refers to the active status of the Concept.
	 */
    public void setActiveStatus(Byte activeStatus) {
        super.setActiveStatus(activeStatus);
    }

    /**
	 * Returns the module Id.
	 * @return moduleId of that concept.
	 */
    public String getModuleId() {
        return super.getModuleId();
    }

    /**
	 * Sets the module Id associated with the Concept.
	 * @param moduleId 	 represents Module Id of the concept
	 * sets moduleId of a concept.
	 */
    public void setModuleId(String moduleId) {
        super.setModuleId(moduleId);
    }

    /**
    * 
    * @return effective time of that component
    */
    public Date getEffectiveTime() {
        return super.getEffectiveTime();
    }

    /**
    * sets the effective time for the component
    * @param effectiveTime - represents effective time
    */
    public void setEffectiveTime(Date effectiveTime) {
        super.setEffectiveTime(effectiveTime);;
    }

    /**
	 * Returns id of the Refset
	 * @return id of a Refset 
	 */
    public String getRefSetId() {
        return refSetId;
    }

    /**
	 * Sets the Refset Id
	 * @param refSetId
	 * 				 Id of a Refset 
	 */
    public void setRefSetId(String refSetId) {
        this.refSetId = refSetId;
    }

    /**
	 * Returns the referenced Component Id.
	 * @return the identifier of the source {@link Concept} of this SimpleMapRefset.
	 */
    public Concept getReferencedComponent() {
        return referencedComponent;
    }

    /**
	 * Sets the source referenced Component Id.
	 * @param referencedComponent
	 * 						 Id of the referencedComponent concept.
	 */
    public void setReferencedComponent(Concept referencedComponent) {
        this.referencedComponent = referencedComponent;
    }

    /**
	 * Returns mapTarget of the SimpleMapRefset
	 * @return mapTarget of a SimpleMapRefset 
	 */
    public String getMapTarget() {
        return mapTarget;
    }

    /**
	 * Sets the SimpleMapRefset mapTarget
	 * @param mapTarget
	 * 				 mapTarget of a SimpleMapRefset 
	 */
    public void setMapTarget(String mapTarget) {
        this.mapTarget = mapTarget;
    }

    
}
