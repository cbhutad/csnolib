package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumRefset {
    
    REFERS_TO("association type","900000000000531004"),
	DESCRIPTION_INACTIVATOR("attribute type","900000000000490003");
	
	private static final Map<String, EnumRefset> lookupForValue = new ConcurrentHashMap<String, EnumRefset>();
	private static final Map<String, EnumRefset> lookupForId = new ConcurrentHashMap<String, EnumRefset>();
	
	static {
        for (EnumRefset d : EnumRefset.values()) {
            lookupForValue.put(d.getValue(), d);
            lookupForId.put(d.getId(), d);
        }
         
    }

	private EnumRefset(String value, String id) {
		this.setValue(value);
		this.setId(id);
	}
    
    private EnumRefset( String value) {
		this.value = value;
	}
    
    private String value;
    private String id;
    
    
    public String getId() {
    	return id;
    }
    public void setId(String id) {
    	this.id=id;
    }
	
    public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
    public static EnumRefset getLookupValue(String value) {
		return lookupForValue.get(value);
	}

	public static EnumRefset getLookupId(String id) {
		return lookupForId.get(id);
	}

}
