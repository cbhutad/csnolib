### getIndexWriter

* returns IndexWriter object. An IndexWriter creates and maintains an index.
* To create an IndexWriter Object we need a directory where indexwriter will write files and IndexWriter configurations. For directory location we use `getIndexDirectoryToWrite` and `getIndexConfig`for writer configurations.

### getIndexDirectoryToWrite

* returns a Directory object. This is the location where the files would be written using IndexWriter
* The passed parameter is refsetId. The various types of refsetId are SimpleMapIndex, SnoIndex, RelIndex, TCIndex, SEMIndex, RelTypeIndex.
* So indexDirectory is calculated as follows:
    if refsetId is null or any of the above mentioned types then directory location points to refsetId file location.
    else it points to the location of REFIndex refsetId type file.

### getIndexConfig

* returns a IndexWriterConfig object.
* To create an IndexWriterConfig object, we need IndexAnalyzer. This is provided using `getAnalyzer`.

### getAnalyzer

* returns an Analyzer object.
* This Analyzer object is created from custom implementation of Analyzer class called `SpecialCharAnalyzer`.