package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumLangRefset {

    UK("uk","900000000000508004"),
	US("us","900000000000509007"),
	HI("hi","672411000189107"),
	CN("cn","722128001"),
	ES("es","448879004"),
	DE("de","722130004"),
	FR("fr","722131000"),
	JA("ja","722129009"),
	SA("sa","997301000189107"),
	TA("ta","997291000189108"),
	UR("ur","997281000189106"),
	MR("mr","997461000189101"),
	ALL("all","all"),
	
	
	SIDDHA_REFSET("997331000189104","ta"),
	SANSKRIT_REFSET("997321000189101","sa"),
	UNANI_REFSET("997341000189105","ur"),
	HINDI_REFSET("683861000189108","hi");
    
	private static final Map<String, EnumLangRefset> lookup = new ConcurrentHashMap<String, EnumLangRefset>();
    
    static {
        for (EnumLangRefset d : EnumLangRefset.values())
            lookup.put(d.getValue(), d);
    }
    	
	private String value;
	private String id;
	
	private EnumLangRefset(String value, String id)
	{
		this.setValue(value);
		this.setId(id);
	}
	
	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Returns SNOMEDCT identifier associated with enumeration.
	 * @return {@link String} value for SNOMEDCT identifier associated with enumeration.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Sets SNOMEDCT identifier for enumeration.
	 * @param id {@link String} value for SNOMEDCT identifier.
	 */
	public void setId(String id) {
		this.id = id;
	}
 
	/**
	 * Returns enumeration for given string value.
	 * @param value {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumLangRefset} enumeration for given string value
	 */
	public static EnumLangRefset getEnum(String value) {
		return lookup.get(value);
	}
	
	 
    public static boolean isValidEnum(String str) {
        for (EnumLangRefset me : EnumLangRefset.values()) {
            if ((me.value).equalsIgnoreCase(str))
                return true;
        }
        return false;
    }

    
}
