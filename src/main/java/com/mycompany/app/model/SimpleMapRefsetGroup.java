package com.mycompany.app.model;

import java.util.List;

public class SimpleMapRefsetGroup {
    
    private List<String> simpleMappedTargetCodes;

    /**
     * Default Constructor
     */
    public SimpleMapRefsetGroup() {
    }

    /**
     * Parameterized Constructor
     * @param simpleMappedTargetCodes           map target simple map codes
     */
    public SimpleMapRefsetGroup(List<String> simpleMappedTargetCodes) {
        this.simpleMappedTargetCodes = simpleMappedTargetCodes;
    }

    /**
	 * Returns the mapped simple map code.
	 * @return
	 * simpleMappedTargetCode
	 */
    public List<String> getSimpleMappedTargetCodes() {
        return simpleMappedTargetCodes;
    }

    /**
	 * Sets the mapped simple map code.
	 * @param simpleMappedTargetCode
	 * simple map code
	 */
    public void setSimpleMappedTargetCodes(List<String> simpleMappedTargetCodes) {
        this.simpleMappedTargetCodes = simpleMappedTargetCodes;
    }

}
