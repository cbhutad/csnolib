package com.mycompany.app.model;

import java.io.Serializable;
import java.util.Date;

import com.mycompany.app.commons.EnumCaseSignificance;
import com.mycompany.app.commons.EnumDescType;

public class Description extends Component implements Serializable {
    
    private static final long serialVersionUID = 1L;

    /**
     * conceptId represents the Id of the concept with which the description is associated
     */
    private String conceptId;

    /**
     * languageCode represents the language used in description text.
     */
    private String languageCode;

    /**
     * typeId represents the id that specifies the description type.
     */
    private EnumDescType typeId;

    /**
     * Represents the Description term.
     */
    private String term;

    /**
     * caseSignificanceId represents the significance of the case in the description.
     */
    private EnumCaseSignificance caseSignificanceId;

    /**
     * Default Constructor
     */
    protected Description() {
    }

    /**
     * Parameterized Constructor
     */
    protected Description(String id, Date effectiveTime, Byte activeStatus, String moduleId, String conceptId, String languageCode, EnumDescType typeId, String term, EnumCaseSignificance caseSignificanceId) {
        super(id,effectiveTime,activeStatus,moduleId);
        this.conceptId = conceptId;
        this.languageCode = languageCode;
        this.typeId = typeId;
        this.term = term;
        this.caseSignificanceId = caseSignificanceId;
    }

    /**
	 * Returns the {@link Concept} identifier.
	 * @return id of that particular Concept.
	 */
    public String getId() {
        return super.getId();
    }

    /**
	 * Sets the Concept identifier.
	 * @param id
	 * 			 represents Concept identifier.
	 */
    public void setId(String id) {
        super.setId(id);
    }

    /**
	 * Returns the active status of the Concept.
	 * @return activeStatus of that concept.
	 */
    public Byte getActiveStatus() {
        return super.getActiveStatus();
    }

    /**
	 * Sets the active status of the Concept.
	 * @param activeStatus			refers to the active status of the Concept.
	 */
    public void setActiveStatus(Byte activeStatus) {
        super.setActiveStatus(activeStatus);
    }

    /**
	 * Returns the module Id.
	 * @return moduleId of that concept.
	 */
    public String getModuleId() {
        return super.getModuleId();
    }

    /**
	 * Sets the module Id associated with the Concept.
	 * @param moduleId 	 represents Module Id of the concept
	 * sets moduleId of a concept.
	 */
    public void setModuleId(String moduleId) {
        super.setModuleId(moduleId);
    }

    /**
    * 
    * @return effective time of that component
    */
    public Date getEffectiveTime() {
        return super.getEffectiveTime();
    }

    /**
    * sets the effective time for the component
    * @param effectiveTime - represents effective time
    */
    public void setEffectiveTime(Date effectiveTime) {
        super.setEffectiveTime(effectiveTime);;
    }

    /**
	 * Returns the Concept Id.
	 * @return the identifier of the associated {@link Concept} described.
	 */
    public String getConceptId() {
        return conceptId;
    }

    /**
	 * Sets the Concept Id attribute of the description.
	 */
    public void setConceptId(String conceptId) {
        this.conceptId = conceptId;
    }

    /**
	  * Returns the languageCode.languageCode that specifies
	  *  the language for the description text.
	  */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
	  * Sets the language Code for the Description.
	  */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
	 * Returns Description type.
	 * @return the type of the description i.e. if the description is either an FSN or a synonym.
	 */
    public String getTypeId() {
        return typeId.getValue();
    }

    /**
	 * Sets the typeId associated with the Description.
	 * @param type
	 * 				the type of the Description. It can be FSN or Synonym.
	 */
    public void setTypeId(EnumDescType typeId) {
        this.typeId = typeId;
    }

    /**
	 * Returns the term corresponding to the Description.
	 * @return the textual description of the associated {@link Concept}.
	 */
    public String getTerm() {
        return term;
    }

    /**
	 * Sets the term for the Description.
	 * @param term
	 * 				 The description text for a particular Description.
	 */
    public void setTerm(String term) {
        this.term = term;
    }

    /**
	 * Returns the Id that represents the case significance of the Description text.
	 * @return caseSignificanceId
	 */
    public String getCaseSignificanceId() {
        return caseSignificanceId.getValue();
    }

    /**
	 * Sets the caseSignificanceId of the description.
	 * @param caseSId
	 * 				 caseSignificance Id that defines the case sensitiveness of the 
	 * 				 Description text.
	 */
    public void setCaseSignificanceId(EnumCaseSignificance caseSignificanceId) {
        this.caseSignificanceId = caseSignificanceId;
    }

    @Override
    public String toString() {
        return this.term;
    }

    @Override
    public int hashCode() {
        int result = 1;
        final int prime = 31;
        result = result * prime + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        Description description = (Description) obj;
        if (id == null && description.id != null)
            return false;
        if(!id.equals(description.id))
            return false;
        return true;
    }
    
}
