package com.mycompany.app.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mycompany.app.commons.Constants;
import com.mycompany.app.commons.EnumComponent;
import com.mycompany.app.util.LoggerUtility;

/**
 * Factory class to generate Components using the
 * <code>getComponent()</code> method.
 */
public class ComponentFactory {
   
    private static final Logger logger = LogManager.getLogger(ComponentFactory.class);
    private static ComponentFactory instance = new ComponentFactory();

    /**
     * Default Constructor
     */
    private ComponentFactory() {
    }

    /**
	 * Function to return the object of the desired Component type.
	 * @param componentType
	 * 						 {@link EnumComponent} value that represents
	 * the type of the Component whose object has to be created.
	 * @param id
	 * 						 represents the Component Id
	 * @return {@link VersionedComponent} It can be an object of {@link Concept}
	 * class or {@link Description} class or {@link Relationship} class.
	 */
    public VersionedComponent getComponent(EnumComponent componentType, String id) {
        if(componentType == null)
            return null;
        if((componentType == EnumComponent.CONCEPT) && (id == null))
            return new Concept();
        else if(componentType == EnumComponent.CONCEPT) {
            try {
                return new Concept(id);
            } catch(Exception ex) {
                StackTraceElement[] arrStackTraceElement = ex.getStackTrace();
                String strMessage = ex.getClass() + Constants.NEW_LINE + Constants.CLASS_NAME + arrStackTraceElement[0].getClassName() + Constants.NEW_LINE + Constants.METHOD_NAME + arrStackTraceElement[0].getMethodName() + Constants.NEW_LINE + Constants.LINE_NUMBER + arrStackTraceElement[0].getLineNumber() + Constants.NEW_LINE + Constants.MESSAGE_DESCRIPTION + ex.getMessage();
                logger.error(LoggerUtility.Exception, strMessage);
                return null;
            }
        } else if((componentType == EnumComponent.DESCRIPTION))
            return new Description();
        else if((componentType == EnumComponent.RELATIONSHIP))
            return new Relationship();
        else if((componentType == EnumComponent.CONCRETERELATIONSHIP))
            return new ConcreteRelationship();
        else if((componentType == EnumComponent.SIMPLEMAP))
            return new SimpleMapRefset();
        return null;
    }

    /**
	 * Static function to return the instance of {@link ComponentFactory} class.
	 * @return instance of {@link ComponentFactory} class.
	 */
    public static ComponentFactory getInstance() {
        return instance;
    }

}
