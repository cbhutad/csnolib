package com.mycompany.app.commons;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public enum EnumDescType {

    FULLY_SPECIFIED_NAME("900000000000003001"),
	SYNONYM("900000000000013009"),
	DEFINITION("900000000000550004");
	
	private static final Map<String, EnumDescType> lookup = new ConcurrentHashMap<String, EnumDescType>();
    
    static {
        for (EnumDescType d : EnumDescType.values())
            lookup.put(d.getValue(), d);
    }
    	
	private String value;
	
	private EnumDescType(String value) {
		this.value = value;
	}
	
	/**
	 * Returns the value associated with enumeration.
	 * @return {@link String} value for enumeration.
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Sets value for enumeration.
	 * @param value {@link String} value to be set.
	 */
	public void setValue(String value) {
		this.value = value;
	}
 
	/**
	 * Returns enumeration for given string value.
	 * @param val {@link String} value for which enumeration is to be retrieve.
	 * @return {@link EnumDescType} enumeration for given string value
	 */
	public static EnumDescType getEnum(String val) {
		return lookup.get(val);
	}
    
}
